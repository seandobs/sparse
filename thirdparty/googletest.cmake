## Downloads and adds the 'gtest' and 'gmock' targets.
## This file is intended to be included in a CMakeLists.txt

include(FetchContent)

# add a fetch target which can be used to retrieve googletest at configure-time
FetchContent_Declare(
  googletest
  GIT_REPOSITORY https://github.com/google/googletest.git
  GIT_TAG        v1.8.x
)

FetchContent_GetProperties(googletest)
if(NOT googletest_POPULATED)
  # fetch googletest
  FetchContent_Populate(googletest)

  # add googletest's CMake configuration so that we can link against the provided 'gtest' and 'gmock' targets
  add_subdirectory(${googletest_SOURCE_DIR} ${googletest_BINARY_DIR})
endif()

# Disable warnings on sign-conversion for the googletest targets
target_compile_options( gtest BEFORE PRIVATE -Wno-sign-conversion )
target_compile_options( gmock BEFORE PRIVATE -Wno-sign-conversion )
