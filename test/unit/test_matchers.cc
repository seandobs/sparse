///
/// @brief Unit tests for the string matchers defined in the `sparse` library
///
/// @file test_matchers.cc
/// @author Sean Dobson (seandobs@gmail.com)
/// @date 2019-02-13
///

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <variant>

#include <sparse/matchers.h>

using namespace sparse;

namespace
{
	template< typename matcher_t >
	void
	expect_matches( const std::vector< field_t >& expected_matches, const matcher_t& matcher, field_t field, bool exit_early = false )
	{
		SCOPED_TRACE( "matcher='" + matcher.domain() + "', field='" + field.to_string() );
		std::vector< field_t > actual_matches;
		matcher.visit(
			field,
			[&]( field_t match )
			{
				actual_matches.emplace_back( match );
				return exit_early;
			}
		);
		ASSERT_EQ( expected_matches.size(), actual_matches.size() );
		EXPECT_EQ( expected_matches, actual_matches );
	}

	template< typename ... val_var, typename matcher_t >
	void
	expect_value_matches( const std::vector< std::tuple< field_t, val_var ... > >& expected_matches, const matcher_t& matcher, field_t field, bool exit_early = false )
	{
		SCOPED_TRACE( "matcher='" + matcher.domain() + "', field='" + field.to_string() + "'" );

		std::vector< std::tuple< field_t, val_var ... > > actual_matches;
		matcher.visit(
			field,
			[&]( field_t match, auto&& ... vals )
			{
				actual_matches.emplace_back( match, std::forward< decltype( vals ) >( vals ) ... );
				return exit_early;
			}
		);
		ASSERT_EQ( expected_matches.size(), actual_matches.size() );
		EXPECT_EQ( expected_matches, actual_matches );
	}

}

TEST( matchers, we_can_match_a_literal )
{
	auto matcher = literal( "some_str" );

	EXPECT_EQ( "some_str", matcher.domain() );

	std::string str1 = "some_str";
	expect_matches(
		{ field_t{ str1.end(), str1.end() } },
		matcher,
		str1
	);

	std::string str2 = "some_other_str";
	expect_matches(
		{},
		matcher,
		str2
	);
}

TEST( matchers, we_can_match_a_single_whitespace )
{
	auto matcher = whitespaces();

	EXPECT_EQ( "\\s", matcher.domain() );

	std::string str1 = " ";
	expect_matches(
		{ { str1.end(), str1.end() } },
		matcher,
		str1
	);

	std::string str2 = "\n";
	expect_matches(
		{ { str2.end(), str2.end() } },
		matcher,
		str2
	);

	std::string str3 = "\t";
	expect_matches(
		{ { str3.end(), str3.end() } },
		matcher,
		str3
	);

	std::string str4 = "\f";
	expect_matches(
		{ { str4.end(), str4.end() } },
		matcher,
		str4
	);

	std::string str5 = "\r";
	expect_matches(
		{ { str5.end(), str5.end() } },
		matcher,
		str5
	);

	std::string str6 = "\v";
	expect_matches(
		{ { str6.end(), str6.end() } },
		matcher,
		str6
	);

	std::string str7 = "  ";
	expect_matches(
		{ { str7.end() - 1, str7.end() } },
		matcher,
		str7
	);

	std::string str8 = "x";
	expect_matches(
		{},
		matcher,
		str8
	);

	std::string str9 = "";
	expect_matches(
		{},
		matcher,
		str9
	);
}

TEST( matchers, we_can_repeat_a_match_between_a_given_min_and_max_number_of_times )
{
	auto matcher = repeat_between( 1u, 4u, literal( "ab" ) );

	EXPECT_EQ( "ab{1,4}", matcher.domain() );

	std::string str1 = "";
	expect_matches(
		{},
		matcher,
		str1
	);

	std::string str2 = "ab";
	expect_matches(
		{
		    {str2.begin() + 2, str2.end()},
		},
		matcher,
		str2
	);

	std::string str3 = "abababab";
	expect_matches(
		{
		    {str3.begin() + 2, str3.end()},
		    {str3.begin() + 4, str3.end()},
		    {str3.begin() + 6, str3.end()},
		    {str3.begin() + 8, str3.end()},
		},
		matcher,
		str3
	);

	std::string str4 = "ababababab";
	expect_matches(
		{
		    {str4.begin() + 2, str4.end()},
		    {str4.begin() + 4, str4.end()},
		    {str4.begin() + 6, str4.end()},
		    {str4.begin() + 8, str4.end()},
		},
		matcher,
		str4
	);
}


TEST( matchers, we_can_repeat_a_match_zero_or_more_times )
{
	auto matcher = repeat_up_to( 2u, literal( "ab" ) );
	EXPECT_EQ( "ab{0,2}", matcher.domain() );

	std::string str1 = "";
	expect_matches(
		{ { str1.begin(), str1.end() } },
		matcher,
		str1
	);

	std::string str2 = "abab";
	expect_matches(
		{
		    { str2.begin(), str2.end() },
		    { str2.begin() + 2, str2.end() },
		    { str2.begin() + 4, str2.end() },
		},
		matcher,
		str2
	);

	std::string str3 = "ababab";
	expect_matches(
		{
		    { str3.begin(), str3.end() },
		    { str3.begin() + 2, str3.end() },
		    { str3.begin() + 4, str3.end() },
		},
		matcher,
		str3
	);
}


TEST( matchers, we_can_early_exit_from_a_repeated_match )
{
	auto matcher = repeat_at_least_once( literal( "a" ) ) + literal( "a" );
	EXPECT_EQ( "a+a", matcher.domain() );

	std::string str1 = "aaaaa";
	{
		std::vector< field_t > matches;
		matcher.visit(
			str1,
			[&]( field_t match )
			{
				matches.emplace_back( match );
				return true;
			}
		);

		std::vector< field_t > expected_matches = { { str1.begin() + 2u, str1.end() } };
		ASSERT_EQ( 1u, matches.size() );
		EXPECT_EQ( expected_matches, matches );
	}
	{
		std::vector< field_t > matches;
		matcher.visit(
			str1,
			[&]( field_t match )
			{
				matches.emplace_back( match );
				return false;
			}
		);

		std::vector< field_t > expected_matches = {
			{ str1.begin() + 2u, str1.end() },
			{ str1.begin() + 3u, str1.end() },
			{ str1.begin() + 4u, str1.end() },
			{ str1.begin() + 5u, str1.end() },

		};
		ASSERT_EQ( 4u, matches.size() );
		EXPECT_EQ( expected_matches, matches );
	}
}

TEST( matchers, we_can_match_an_unsigned_int_parameter )
{
	auto matcher = param< unsigned int >();
	EXPECT_EQ( "\\d+", matcher.domain() );

	std::string str1 = "123";
	expect_value_matches< unsigned int >(
		{ { field_t{str1.end(), str1.end()}, 123u } },
		matcher,
		str1
	);

	std::string str2 = "321  ";
	expect_value_matches< unsigned int >(
		{ { field_t{str2.begin() + 3, str2.end()}, 321u } },
		matcher,
		str2
	);

	std::string str3 = " 123";
	expect_value_matches< unsigned int >(
		{},
		matcher,
		str3
	);

	std::string str4 = "a123";
	expect_value_matches< unsigned int >(
		{},
		matcher,
		str4
	);

	std::string str5 = "123a123";
	expect_value_matches< unsigned int >(
		{ { field_t{str5.begin() + 3, str5.end()}, 123u } },
		matcher,
		str5
	);

	std::string str6 = "123 321";
	expect_value_matches< unsigned int >(
		{ { field_t{str6.begin() + 3, str6.end()}, 123u } },
		matcher,
		str6
	);

	std::string str7 = "";
	expect_value_matches< unsigned int >(
		{},
		matcher,
		str7
	);

	std::string str8 = "-123";
	expect_value_matches< unsigned int >(
		{},
		matcher,
		str8
	);

	std::string str9 = "+123";
	expect_value_matches< unsigned int >(
		{ { field_t{ str9.end(), str9.end() }, 123u } },
		matcher,
		str9
	);

	std::string str10 = "12356789101112232425262718192021222324";
	expect_value_matches< unsigned int >(
		{},
		matcher,
		str10
	);
}

TEST( matchers, we_can_match_an_int_parameter )
{
	auto matcher = param< int >();
	EXPECT_EQ( "(\\+|-)?\\d+", matcher.domain() );

	std::string str1 = "123";
	expect_value_matches< int >(
		{ { field_t{str1.end(), str1.end()}, 123 } },
		matcher,
		str1
	);

	std::string str2 = "321  ";
	expect_value_matches< int >(
		{ { field_t{str2.begin() + 3, str2.end()}, 321 } },
		matcher,
		str2
	);

	std::string str3 = " 123";
	expect_value_matches< int >(
		{},
		matcher,
		str3
	);

	std::string str4 = "a123";
	expect_value_matches< int >(
		{},
		matcher,
		str4
	);

	std::string str5 = "123a123";
	expect_value_matches< int >(
		{ { field_t{str5.begin() + 3, str5.end()}, 123 } },
		matcher,
		str5
	);

	std::string str6 = "123 321";
	expect_value_matches< int >(
		{ { field_t{str6.begin() + 3, str6.end()}, 123 } },
		matcher,
		str6
	);

	std::string str7 = "";
	expect_value_matches< int >(
		{},
		matcher,
		str7
	);

	std::string str8 = "-123";
	expect_value_matches< int >(
		{ { field_t{ str8.end(), str8.end() }, -123 } },
		matcher,
		str8
	);

	std::string str9 = "+123";
	expect_value_matches< int >(
		{ { field_t{ str9.end(), str9.end() }, 123 } },
		matcher,
		str9
	);

	std::string str10 = "12356789101112232425262718192021222324";
	expect_value_matches< int >(
		{},
		matcher,
		str10
	);
}

TEST( matchers, we_can_match_a_string_parameter )
{
	auto matcher = dquoted( param< std::string >() );
	EXPECT_EQ( "(\"(.|\\n)*\"&&^(\"(.|\\n)*\"(.|\\n)*\"))", matcher.domain() );

	std::string str1 = "\"mystr\"";
	expect_value_matches< std::string >(
		{ { field_t{str1.end(), str1.end()}, "mystr" } },
		matcher,
		str1,
		true
	);

	std::string str2 = "\"mystr\"  ";
	expect_value_matches< std::string >(
		{ { field_t{str2.begin() + 7, str2.end()}, "mystr" } },
		matcher,
		str2,
		true
	);

	std::string str3 = " \"mystr\"";
	expect_value_matches< std::string >(
		{},
		matcher,
		str3,
		true
	);

	std::string str4 = "a\"mystr\"";
	expect_value_matches< std::string >(
		{},
		matcher,
		str4,
		true
	);

	std::string str5 = "\"mystr\"a\"myotherstr\"";
	expect_value_matches< std::string >(
		{ { field_t{str5.begin() + 7, str5.end()}, "mystr" } },
		matcher,
		str5,
		true
	);

	std::string str6 = "\"mystr\" \"myotherstr\"";
	expect_value_matches< std::string >(
		{ { field_t{str6.begin() + 7, str6.end()}, "mystr" } },
		matcher,
		str6,
		true
	);

	std::string str7 = "";
	expect_value_matches< std::string >(
		{},
		matcher,
		str7,
		true
	);

	std::string str8 = "\"mystr";
	expect_value_matches< std::string >(
		{},
		matcher,
		str8,
		true
	);

	std::string str9 = "mystr\"";
	expect_value_matches< std::string >(
		{},
		matcher,
		str9,
		true
	);

	std::string str10 = "\"mystr \n myotherstr\"";
	expect_value_matches< std::string >(
		{ { field_t{str10.end(), str10.end()}, "mystr \n myotherstr" } },
		matcher,
		str10,
		true
	);

	std::string str11 = "\"\"";
	expect_value_matches< std::string >(
		{ { field_t{str11.end(), str11.end()}, "" } },
		matcher,
		str11,
		true
	);

	std::string str12 = "\"my\\\"str\"  ";
	expect_value_matches< std::string >(
		{ { field_t{str12.begin() + 9, str12.end()}, "my\"str" } },
		matcher,
		str12,
		true
	);
}

TEST( matchers, we_can_match_an_assignment )
{
	auto matcher = assignment( "myparam", param< unsigned int >() );
	EXPECT_EQ( "myparam\\s*=\\s*\\d+", matcher.domain() );

	std::string str1 = "myparam=123";
	expect_value_matches< unsigned int >(
		{ { field_t{str1.end(), str1.end()}, 123u } },
		matcher,
		str1,
		true
	);

	std::string str2 = "myparam = 321  ";
	expect_value_matches< unsigned int >(
		{ { field_t{str2.begin() + 13, str2.end()}, 321u } },
		matcher,
		str2,
		true
	);

	std::string str3 = "myparam =123";
	expect_value_matches< unsigned int >(
		{ { field_t{str3.begin() + 12, str3.end()}, 123u } },
		matcher,
		str3,
		true
	);

	std::string str4 = "myparam= 123";
	expect_value_matches< unsigned int >(
		{ { field_t{str4.begin() + 12, str4.end()}, 123u } },
		matcher,
		str4,
		true
	);

	std::string str5 = "123";
	expect_value_matches< unsigned int >(
		{},
		matcher,
		str5,
		true
	);

	std::string str6 = "myparam=";
	expect_value_matches< unsigned int >(
		{},
		matcher,
		str6,
		true
	);

	std::string str7 = "";
	expect_value_matches< unsigned int >(
		{},
		matcher,
		str7,
		true
	);

	std::string str8 = "someotherparam=123";
	expect_value_matches< unsigned int >(
		{},
		matcher,
		str8,
		true
	);
}

TEST( matchers, we_can_require_a_match )
{
	auto matcher = require( literal( "foo" ) );
	EXPECT_EQ( "foo", matcher.domain() );

	std::string str1 = "foo";
	EXPECT_NO_THROW(
		expect_matches(
			{ field_t{str1.end(), str1.end()} },
			matcher,
			str1,
			true
		);
	);

	std::string str2 = "not_foo";
	EXPECT_THROW(
		matcher.visit(
			str2,
			[]( field_t )
			{
				return true;
			}
		),
		sparse::parse_exception
	);
}

TEST( matchers, we_can_match_the_end_of_a_string )
{
	auto matcher = literal( "a" ) + end_of_field();
	EXPECT_EQ( "a$", matcher.domain() );

	std::string str1 = "a";
	expect_matches(
		{ { str1.end(), str1.end() } },
		matcher,
		str1
	);

	std::string str2 = "aa";
	expect_matches(
		{},
		matcher,
		str2
	);

	std::string str3 = "";
	expect_matches(
		{},
		matcher,
		str3
	);
}

TEST( matchers, we_can_match_an_empty_string )
{
	auto matcher = end_of_field();
	EXPECT_EQ( "$", matcher.domain() );

	std::string str1 = "";
	expect_matches(
		{ { str1.end(), str1.end() } },
		matcher,
		str1
	);

	std::string str2 = "a";
	expect_matches(
		{},
		matcher,
		str2
	);
}

TEST( matchers, we_can_compose_matchers_via_the_join_operator )
{
	auto matcher = literal( "foo" ) + repeat_any( literal( " " ) ) + literal( "bar" ) + repeat_any( literal( " " ) ) + literal( "dog" ) + end_of_field();
	EXPECT_EQ( "foo *bar *dog$", matcher.domain() );

	std::string str1 = "foo bar   dog";
	expect_matches(
		{ {str1.end(), str1.end()} },
		matcher,
		str1
	);

	std::string str2 = "foo   bar dog";
	expect_matches(
		{ {str2.end(), str2.end()} },
		matcher,
		str2
	);

	std::string str3 = "foo";
	expect_matches(
		{},
		matcher,
		str3
	);
}

TEST( matchers, we_can_compose_matchers_via_the_or_operator_to_match_any_alternative )
{
	auto matcher = ( literal( "foo" ) | ( literal( "bar" ) | literal( "dog" ) ) ) + end_of_field();
	EXPECT_EQ( "(foo|bar|dog)$", matcher.domain() );

	std::string str1 = "foo";
	expect_matches(
		{ {str1.end(), str1.end()} },
		matcher,
		str1
	);

	std::string str2 = "bar";
	expect_matches(
		{ {str2.end(), str2.end()} },
		matcher,
		str2
	);

	std::string str3 = "dog";
	expect_matches(
		{ {str3.end(), str3.end()} },
		matcher,
		str3
	);

	std::string str4 = "cat";
	expect_matches(
		{},
		matcher,
		str4
	);

	std::string str5 = "foobar";
	expect_matches(
		{},
		matcher,
		str5
	);
}

TEST( matchers, we_can_compose_matchers )
{
	auto matcher =
		( literal( "foo" ) + ws() + literal( "bar" ) + ws()
		  + ( literal( "dog" ) | literal( "cat" ) ) )
		| ( literal( "mouse" ) );
	EXPECT_EQ( "(foo\\s*bar\\s*(dog|cat)|mouse)", matcher.domain() );

	std::string str1 = "foo bar   dog";
	expect_matches(
		{ {str1.end(), str1.end()} },
		matcher,
		str1
	);

	std::string str2 = "foo   bar cat";
	expect_matches(
		{ {str2.end(), str2.end()} },
		matcher,
		str2
	);

	std::string str3 = "mouse";
	expect_matches(
		{ {str3.end(), str3.end()} },
		matcher,
		str3
	);
}

TEST( matchers, we_can_compose_parameter_matchers_via_the_join_operator_to_match_all_alternatives_in_sequential_order )
{
	auto matcher = param< int >() + ws() + param< unsigned int >() + literal( "," ) + dquoted( param< std::string >() );
	EXPECT_EQ( "(\\+|-)?\\d+\\s*\\d+,(\"(.|\\n)*\"&&^(\"(.|\\n)*\"(.|\\n)*\"))", matcher.domain() );

	std::string str1 = "-123   456,\"abc\"";
	expect_value_matches< std::tuple< int, unsigned int, std::string > >(
		{ { field_t{str1.end(), str1.end()}, { -123, 456u, "abc" } } },
		matcher,
		str1,
		true
	);
}

TEST( matchers, we_can_compose_parameter_matchers_via_the_bitwise_or_operator_to_match_any_alternative )
{
	auto matcher = param< unsigned int >() | param< int >() | ( dquoted( param< std::string >() ) );
	EXPECT_EQ( "(\\d+|(\\+|-)?\\d+|(\"(.|\\n)*\"&&^(\"(.|\\n)*\"(.|\\n)*\")))", matcher.domain() );

	std::string str1 = "123";
	expect_value_matches< std::variant< unsigned int, int, std::string > >(
		{ { field_t{ str1.end(), str1.end() }, static_cast< unsigned int >( 123u )} },
		matcher,
		str1,
		true
	);

	std::string str2 = "-123";
	expect_value_matches< std::variant< unsigned int, int, std::string > >(
		{ { field_t{ str2.end(), str2.end() }, static_cast< int >( -123 )} },
		matcher,
		str2,
		true
	);

	std::string str3 = "\"abc\"";
	expect_value_matches< std::variant< unsigned int, int, std::string > >(
		{ { field_t{ str3.end(), str3.end() }, "abc" } },
		matcher,
		str3,
		true
	);

	std::string str4 = "foo";
	expect_value_matches< std::variant< unsigned int, int, std::string > >(
		{},
		matcher,
		str4,
		true
	);
}

TEST( matchers, we_can_compose_parameter_matchers_via_the_bitwise_and_operator_to_match_all_alternatives_in_any_order )
{
	auto comma_delim = ws() + literal( "," ) + ws();
	auto matcher     =
		delimited_unordered_join(
			comma_delim,
			param< int >() + literal( "i" ),
			delimited_unordered_join(
				comma_delim,
				param< int >() + literal( "j" ),
				param< int >() + literal( "k" )
			),
			param< unsigned int >() + literal( "u" ),
			dquoted( param< std::string >() )
		)
		+ end_of_field();

	EXPECT_EQ( "&(\\s*,\\s*)[(\\+|-)?\\d+i:&(\\s*,\\s*)[(\\+|-)?\\d+j:(\\+|-)?\\d+k]:\\d+u:(\"(.|\\n)*\"&&^(\"(.|\\n)*\"(.|\\n)*\"))]$", matcher.domain() );

	std::string str1 = "-123i,456j,789k,101112u,\"abc\"";
	expect_value_matches< std::tuple< int, std::tuple< int, int >, unsigned int, std::string > >(
		{ { field_t{str1.end(), str1.end()}, { -123, {456, 789}, 101112u, "abc" } } },
		matcher,
		str1,
		true
	);

	std::string str2 = "\"abc\", -123i ,101112u, 789k, 456j";
	expect_value_matches< std::tuple< int, std::tuple< int, int >, unsigned int, std::string > >(
		{ { field_t{str2.end(), str2.end()}, { -123, {456, 789}, 101112u, "abc" } } },
		matcher,
		str2,
		true
	);

	std::string str3 = "456j,\"abc\",-123i,101112u,789k"; // No match because j and k are separated
	expect_value_matches< std::tuple< int, std::tuple< int, int >, unsigned int, std::string > >(
		{},
		matcher,
		str3,
		true
	);

	std::string str4 = "\"abc\", -123i101112u , 456j, 789k"; // No match because missing delimiter
	expect_value_matches< std::tuple< int, std::tuple< int, int >, unsigned int, std::string > >(
		{},
		matcher,
		str4,
		true
	);

	std::string str5 = ",\"abc\", -123i, 101112u , 456j, 789k"; // No match because extra preceding delimiter
	expect_value_matches< std::tuple< int, std::tuple< int, int >, unsigned int, std::string > >(
		{},
		matcher,
		str5,
		true
	);

	std::string str6 = "\"abc\", -123i, 101112u , 456j, 789k "; // No match because of extra chars at the end of the field
	expect_value_matches< std::tuple< int, std::tuple< int, int >, unsigned int, std::string > >(
		{},
		matcher,
		str6,
		true
	);

	std::string str7 = "\"abc\", -123i, 101112u , 456j, 789k,"; // No match because of extra delimiter at the end of the field
	expect_value_matches< std::tuple< int, std::tuple< int, int >, unsigned int, std::string > >(
		{},
		matcher,
		str7,
		true
	);
}

TEST( matchers, we_can_translate_a_match_to_a_value )
{
	auto matcher = translate( literal( "foo" ), size_t{ 123u } )
	               | translate( literal( "bar" ), std::string{ "str" } );
	EXPECT_EQ( "(foo|bar)", matcher.domain() );

	std::string mystr = "bar";
	matcher.visit(
		mystr,
		[]( field_t, auto&& my_thing )
		{
			if constexpr( std::is_same_v< std::decay_t< decltype( my_thing ) >, size_t > )
			{
			    std::cout << my_thing + 10u;
			}
			else
			{
			    std::cout << my_thing + " is a string!";
			}
			std::cout << std::endl;
			return true;
		}
	);

	std::string str1 = "foo";
	expect_value_matches< std::variant< size_t, std::string > >(
		{ { field_t{str1.end(), str1.end()}, 123u } },
		matcher,
		str1,
		true
	);

	std::string str2 = "bar";
	expect_value_matches< std::variant< size_t, std::string > >(
		{ { field_t{str2.end(), str2.end()}, "str" } },
		matcher,
		str2,
		true
	);

	std::string str3 = "cat";
	expect_value_matches< std::variant< size_t, std::string > >(
		{},
		matcher,
		str3,
		true
	);
}

TEST( matchers, we_can_match_a_function_call )
{
	enum class mock_option_e
	{
		OPTION1,
		OPTION2,
		OPTION3
	};
	auto comma_delim = ws() + literal( "," ) + ws();
	auto matcher     =
		literal( "my_fun" ) + ws() + literal( "(" ) + ws()
		+ delimited_unordered_join(
			comma_delim,
			assignment( "p1", param< int >() ),
			assignment( "p2", param< unsigned int >() ) | translate( literal( "" ), static_cast< unsigned int >( 987u ) ),
			assignment( "p3", dquoted( param< std::string >() ) ),
			assignment( "p4", translate( literal( "o1" ), mock_option_e::OPTION1 ) | translate( literal( "o2" ), mock_option_e::OPTION2 ) | translate( literal( "o3" ), mock_option_e::OPTION3 ) )
		)
		+ ( ws() + literal( ")" ) + end_of_field() );

	EXPECT_EQ( "my_fun\\s*(\\s*&(\\s*,\\s*)[p1\\s*=\\s*(\\+|-)?\\d+:(p2\\s*=\\s*\\d+|):p3\\s*=\\s*(\"(.|\\n)*\"&&^(\"(.|\\n)*\"(.|\\n)*\")):p4\\s*=\\s*(o1|o2|o3)]\\s*)$", matcher.domain() );

	std::string str1 = "my_fun( p1 = -123, p2 = 456, p3 = \"abc\", p4 = o1 )";
	expect_value_matches< std::tuple< int, unsigned int, std::string, mock_option_e > >(
		{ { field_t{str1.end(), str1.end()}, { -123, 456u, "abc", mock_option_e::OPTION1 } } },
		matcher,
		str1,
		true
	);

	std::string str2 = "my_fun( p2=789, p4 = o2, p3=\"cba\", p1=321 )";
	expect_value_matches< std::tuple< int, unsigned int, std::string, mock_option_e > >(
		{ { field_t{str2.end(), str2.end()}, { 321, 789u, "cba", mock_option_e::OPTION2 } } },
		matcher,
		str2,
		true
	);

	std::string str3 = "my_fun( p4 = o2, p3=\"cba\", p1=321 )";
	expect_value_matches< std::tuple< int, unsigned int, std::string, mock_option_e > >(
		{ { field_t{str3.end(), str3.end()}, { 321, 987u, "cba", mock_option_e::OPTION2 } } },
		matcher,
		str3,
		true
	);
}

TEST( matchers, we_can_match_a_function_call2 )
{
	enum class mock_option_e
	{
		OPTION1,
		OPTION2,
		OPTION3
	};
	auto comma_delim = ws() + literal( "," ) + ws();
	auto matcher     =
		function(
			"my_fun",
			std::make_tuple(
				arg( "p1", param< int >() ),
				arg( "p2", param< unsigned int >() ),
				arg( "p3", dquoted( param< std::string >() ) )
			),
			std::make_tuple(
				defaulted_arg(
					"p4",
					translate( literal( "o1" ), mock_option_e::OPTION1 ) | translate( literal( "o2" ), mock_option_e::OPTION2 ) | translate( literal( "o3" ), mock_option_e::OPTION3 ),
					mock_option_e::OPTION2
				)
			)
		)
		+ end_of_field();

	EXPECT_EQ( "my_fun\\s*(\\s*((\\+|-)?\\d+\\s*,\\s*\\d+\\s*,\\s*(\"(.|\\n)*\"&&^(\"(.|\\n)*\"(.|\\n)*\"))\\s*,\\s*(o1|o2|o3)|&(\\s*,\\s*)[p1\\s*=\\s*(\\+|-)?\\d+:p2\\s*=\\s*\\d+:p3\\s*=\\s*(\"(.|\\n)*\"&&^(\"(.|\\n)*\"(.|\\n)*\")):(p4\\s*=\\s*(o1|o2|o3)|)])\\s*)$", matcher.domain() );

	std::string str1 = "my_fun( p1 = -123, p2 = 456, p3 = \"abc\", p4 = o1 )";
	expect_value_matches< std::tuple< int, unsigned int, std::string, mock_option_e > >(
		{ { field_t{str1.end(), str1.end()}, { -123, 456u, "abc", mock_option_e::OPTION1 } } },
		matcher,
		str1,
		true
	);

	std::string str2 = "my_fun( p2=789, p4 = o2, p3=\"cba\", p1=321 )";
	expect_value_matches< std::tuple< int, unsigned int, std::string, mock_option_e > >(
		{ { field_t{str2.end(), str2.end()}, { 321, 789u, "cba", mock_option_e::OPTION2 } } },
		matcher,
		str2,
		true
	);

	std::string str3 = "my_fun( p2 = 987, p3=\"cba\", p1=321 )";
	expect_value_matches< std::tuple< int, unsigned int, std::string, mock_option_e > >(
		{ { field_t{str3.end(), str3.end()}, { 321, 987u, "cba", mock_option_e::OPTION2 } } },
		matcher,
		str3,
		true
	);

	std::string str4 = "my_fun( -123, 987, \"cba\", o3 )";
	expect_value_matches< std::tuple< int, unsigned int, std::string, mock_option_e > >(
		{ { field_t{str4.end(), str4.end()}, { -123, 987u, "cba", mock_option_e::OPTION3 } } },
		matcher,
		str4,
		true
	);
}