///
/// @brief Unit tests for the cmdline args parser defined in the `sparse` library
///
/// @file test_cmdline_args_t.cc
/// @author Sean Dobson (seandobs@gmail.com)
/// @date 2019-02-20
///

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <sparse/cmdline_args_t.h>
#include <sparse/parse_exception.h>

using namespace sparse;


TEST( cmdline_args_t, we_can_longform_pop_parameters )
{
	const std::string longform = "myparam";

	{
		auto value = cmdline_args_t{
			"",
			{ "--myparam", "my value", "--my_other_param", "my other value" }
		}.pop_param( longform );
		ASSERT_TRUE( value );
		EXPECT_EQ( "my value", *value );
	}
}

TEST( cmdline_args_t, we_can_pop_shortform_parameters )
{
	const char shortform = 'm';

	{
		auto value = cmdline_args_t{
			"",
			{ "-m", "my value" }
		}.pop_param( "", shortform );
		ASSERT_TRUE( value );
		EXPECT_EQ( "my value", *value );
	}
}

TEST( cmdline_args_t, we_return_an_empty_result_when_no_arg_is_matched_to_a_requested_parameter )
{
	const std::string longform = "myparam";
	const char shortform       = 'm';

	{
		auto value = cmdline_args_t{
			"",
			{ "--my_other_param", "my other value", "-o", "my another other value" }
		}.pop_param( longform, shortform );
		EXPECT_FALSE( value );
	}
}

TEST( cmdline_args_t, we_throw_a_parse_exception_when_a_required_param_is_missing )
{
	const std::string longform = "myparam";
	const char shortform       = 'm';

	{
		cmdline_args_t cmdline_args{
			"",
			{ "--my_other_param", "my other value", "-o", "my another other value" }
		};
		EXPECT_THROW(
			cmdline_args.pop_required_param( longform, shortform ),
			parse_exception
		);
	}

	{
		cmdline_args_t cmdline_args{
			"",
			{ "--myparam", "my value" }
		};
		EXPECT_NO_THROW(
			cmdline_args.pop_required_param( longform, shortform )
		);
	}
}