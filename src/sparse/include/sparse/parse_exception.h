///
/// @brief A `parse_exception` is thrown when any errors occur while parsing an inputted field.
///
/// @file parse_exception.h
/// @author Sean Dobson, seandobs@gmail.com
/// @date 2019-02-20
///

#pragma once

#include <stdexcept>

#include "field_t.h"

namespace sparse
{
	class parse_exception : public std::runtime_error
	{
	  public:
		parse_exception( const std::string& field_str, const std::string& domain_str, const std::string& reason );

		const std::string& get_field() const;
		const std::string& get_domain() const;
		const std::string& get_reason() const;

	  private:
		std::string parse_str;
		std::string domain_str;
		std::string reason;
	};
} //namespace sparse
