///
/// @brief A library of composable string parsing matchers which implement the visitor pattern.
///
/// @file matchers.h
/// @author Sean Dobson, seandobs@gmail.com
/// @date 2018-04-18
///

#pragma once

#include <string>
#include <sstream>
#include <tuple>
#include <utility>
#include <limits>
#include <cctype>
#include <list>
#include <vector>

#include <cassert>
#include <cerrno>
#include <cstdlib>

#include "field_t.h"
#include "parse_exception.h"

namespace sparse
{
	namespace detail
	{

		///@{
		/// Forward declarations our compositional matchers so that we can compose them recursively.

		template< typename first_matcher_t, typename ... matcher_var >
		struct join_t;

		template< typename ... matcher_var >
		struct or_t;
		///@}
	}

	///
	/// @brief Implements `operator+` for matchers, see `matcher_base_t::operator+` for details.
	///
	template< typename ... matcher_var >
	auto
	join( matcher_var&& ... matchers )
	{
		return detail::join_t< std::decay_t< matcher_var > ... >{ std::forward< matcher_var >( matchers ) ... };
	}

	template< typename left_t, typename right_t >
	auto
	join( left_t && l, right_t&& r )
	{
		return detail::join_t< std::decay_t< left_t >, std::decay_t< right_t > >{ std::forward< left_t >( l ), std::forward< right_t >( r ) };
	}
	template< typename left_t, typename ... right_var >
	auto
	join( left_t && l, detail::join_t< right_var ... >&& r )
	{
		return detail::join_t< std::decay_t< left_t >, right_var ... >{ std::forward< left_t >( l ), std::move( r ) };
	}
	template< typename left_t, typename ... right_var >
	auto
	join( left_t && l, const detail::join_t< right_var ... >& r )
	{
		return detail::join_t< std::decay_t< left_t >, right_var ... >{ std::forward< left_t >( l ), r };
	}

	namespace detail
	{
		///@{
		///
		/// @brief Implements `operator|` for matchers, see `matcher_base_t::operator|` for details.
		///
		template< typename left_t, typename right_t >
		auto
		_or( left_t && l, right_t&& r )
		{
			return detail::or_t< std::decay_t< left_t >, std::decay_t< right_t > >{ std::forward< left_t >( l ), std::forward< right_t >( r ) };
		}
		template< typename left_t, typename ... right_var >
		auto
		_or( left_t && l, detail::or_t< right_var ... >&& r )
		{
			return detail::or_t< std::decay_t< left_t >, right_var ... >{ std::forward< left_t >( l ), std::move( r ) };
		}
		template< typename left_t, typename ... right_var >
		auto
		_or( left_t && l, const detail::or_t< right_var ... >& r )
		{
			return detail::or_t< std::decay_t< left_t >, right_var ... >{ std::forward< left_t >( l ), r };
		}
		///@}

		///
		/// @brief A base class from which all matchers should inherit.
		///
		///  This automatically extends derived class with an implementation for `operator+` and `operator|`
		///  that let us compose matchers via join/or respectively.
		///
		///  This is an example of the CRTP pattern where we _know_  that this class will be the base class of the template parameter,
		///  and therefore in the implementation of its member functions we can statically cast this instance to an instance of the derived class.
		///
		/// @tparam derived_t
		///  A class which inherits from this one (e.g. `class my_matcher_t : public matcher_base_t< my_matcher_t >`)
		///
		template< typename derived_t >
		struct matcher_base_t
		{

			///@{
			///
			/// @brief Composes this matcher with another such that a match is found if:
			/// - this matcher matches a prefix of the given field,
			/// - and then the other matcher matches a prefix of the remaining portion of the field.
			///
			/// @param other
			///  Some other matcher
			///
			/// @return auto
			///  A matcher that will match a field to this matcher followed by the given matcher.
			///
			template< typename other_fw >
			auto
			operator+( other_fw&& other ) const &
			{
				return join( static_cast< const derived_t & >( *this ), std::forward< other_fw >( other ) );
			}
			template< typename other_fw >
			auto
			operator+( other_fw&& other ) &&
			{
				return join( static_cast< derived_t && >( *this ), std::forward< other_fw >( other ) );
			}
			///@}

			///@{
			///
			/// @brief Composes this matcher with another such that a match is found if:
			/// - this matcher matches a prefix of the given field,
			/// - or the other matcher matches a prefix of the given field.
			///
			/// @param other
			///  Some other matcher
			///
			/// @return auto
			///  A matcher.
			///
			template< typename other_fw >
			auto
			operator|( other_fw&& other ) const &
			{
				return _or( static_cast< const derived_t & >( *this ), std::forward< other_fw >( other ) );
			}
			template< typename other_fw >
			auto
			operator|( other_fw&& other ) &&
			{
				return _or( static_cast< derived_t && >( *this ), std::forward< other_fw >( other ) );
			}
			///@}
		};

		template< typename first_matcher_t, typename ... matcher_var >
		class join_t : public matcher_base_t< join_t< first_matcher_t, matcher_var ... > >
		{
		  public:
			template< typename ... l_matcher_var, typename ... r_matcher_var >
			join_t( join_t< first_matcher_t, l_matcher_var ... > l, join_t< r_matcher_var ... > r )
				: matchers{ std::tuple_cat( std::move( l.matchers ), std::move( r.matchers ) ) }
			{}

			template< typename last_matcher_t, typename ... middle_matcher_var >
			join_t( join_t< first_matcher_t, middle_matcher_var ... > l, last_matcher_t r )
				: matchers{ std::tuple_cat( std::move( l.matchers ), std::make_tuple( std::move( r ) ) ) }
			{}

			join_t( first_matcher_t l, join_t< matcher_var ... > r, std::enable_if_t< sizeof ... ( matcher_var ) != 0u > * = nullptr )
				: matchers{ std::tuple_cat( std::make_tuple( std::move( l ) ), std::move( r.matchers ) ) }
			{}

			join_t( first_matcher_t l, matcher_var ... matchers )
				: matchers{ std::move( l ), std::move( matchers ) ... }
			{}

			join_t( const join_t& ) = default;
			join_t( join_t&& )      = default;

			template< typename op_fn >
			bool
			visit( field_t field, op_fn && op ) const
			{
				return _visit(
					field,
					[&]( field_t unmatched, auto&& ... vals )
					{
						if constexpr( std::bool_constant< ( sizeof ... ( vals ) > 1u ) >::value )
						{
						    return std::forward< op_fn >( op )( unmatched, std::forward_as_tuple( std::forward< decltype( vals ) >( vals ) ... ) );
						}
						else
						{
						    return std::forward< op_fn >( op )( unmatched, std::forward< decltype( vals ) >( vals ) ... );
						}
					},
					std::make_index_sequence< 1u + ( sizeof ... ( matcher_var ) ) >{}
				);
			}

			std::string
			domain() const
			{
				return _domain_builder( std::make_index_sequence< 1u + sizeof ... ( matcher_var ) >{} );
			}

			template< typename ... r_matcher_var >
			auto
			operator+( join_t< r_matcher_var ... > r ) const &
			{
				return join_t< first_matcher_t, matcher_var ..., r_matcher_var ... >{ *this, std::move( r ) };
			}

			template< typename ... r_matcher_var >
			auto
			operator+( join_t< r_matcher_var ... > r ) &&
			{
				return join_t< first_matcher_t, matcher_var ..., r_matcher_var ... >{ std::move( *this ), std::move( r ) };
			}

			template< typename r_matcher_fw >
			auto
			operator+( r_matcher_fw&& r ) const &
			{
				return join_t< first_matcher_t, matcher_var ..., std::decay_t< r_matcher_fw > >{ *this, std::forward< r_matcher_fw >( r ) };
			}

			template< typename r_matcher_fw >
			auto
			operator+( r_matcher_fw&& r ) &&
			{
				return join_t< first_matcher_t, matcher_var ..., std::decay_t< r_matcher_fw > >{ std::move( *this ), std::forward< r_matcher_fw >( r ) };
			}

		  private:


			template< size_t current_i, size_t ... i_var >
			std::string
			_domain_builder( std::index_sequence< current_i, i_var ... > ) const
			{
				return std::get< current_i >( matchers ).domain() + _domain_builder( std::index_sequence< i_var ... >{} );
			}

			template< size_t last_i >
			std::string
			_domain_builder( std::index_sequence< last_i > ) const
			{
				return std::get< last_i >( matchers ).domain() + _domain_builder( std::index_sequence< >{} );
			}

			std::string
			_domain_builder( std::index_sequence< > ) const
			{
				return "";
			}

			template< typename op_fn, size_t current_i, size_t ... i_var >
			bool
			_visit( field_t field, op_fn && op, std::index_sequence< current_i, i_var ... > ) const
			{
				// Try to visit using the i'th matcher
				return std::get< current_i >( matchers ).visit(
					field,
					[&]( field_t unmatched, auto&& ... i_vals )
					{
						return _visit(
							unmatched,
							[&]( field_t final_unmatched, auto&& ... remaining_vals )
							{
								return std::forward< op_fn >( op )( final_unmatched, std::forward< decltype( i_vals ) >( i_vals ) ..., std::forward< decltype( remaining_vals ) >( remaining_vals ) ... );
							},
							std::index_sequence< i_var ... >{}
						);

					}
				);
			}

			template< typename op_fn >
			bool
			_visit( field_t field, op_fn && op, std::index_sequence< > ) const
			{
				return std::forward< op_fn >( op )( field );
			}

			std::tuple< first_matcher_t, matcher_var ... > matchers;

			// Make all join_t< ... > friends of this class so they have access to private members
			template< typename, typename ... >
			friend class join_t;
		};

		template< typename ... matcher_var >
		class or_t : public matcher_base_t< or_t< matcher_var ... > >
		{
		  public:

			template< typename ... l_matcher_var, typename r_matcher_fw >
			or_t( const or_t< l_matcher_var ... >& l, r_matcher_fw&& r )
				: matchers{ std::tuple_cat( l.matchers, std::make_tuple( std::forward< r_matcher_fw >( r ) ) ) }
			{}

			template< typename ... l_matcher_var, typename r_matcher_fw >
			or_t( or_t< l_matcher_var ... >&& l, r_matcher_fw&& r )
				: matchers{ std::tuple_cat( std::move( l.matchers ), std::make_tuple( std::forward< r_matcher_fw >( r ) ) ) }
			{}

			template< typename l_matcher_fw, typename ... r_matcher_var >
			or_t( l_matcher_fw&& l, const or_t< r_matcher_var ... >& r )
				: matchers{ std::tuple_cat( std::make_tuple( std::forward< l_matcher_fw >( l ) ), r.matchers ) }
			{}

			template< typename l_matcher_fw, typename ... r_matcher_var >
			or_t( l_matcher_fw&& l, or_t< r_matcher_var ... >&& r )
				: matchers{ std::tuple_cat( std::make_tuple( std::forward< l_matcher_fw >( l ) ), std::move( r.matchers ) ) }
			{}

			or_t( matcher_var ... matchers )
				: matchers{ std::move( matchers ) ... }
			{}

			or_t( const or_t& ) = default;
			or_t( or_t&& )      = default;

			template< typename op_fn >
			bool
			visit( field_t field, op_fn && op ) const
			{
				return _visit(
					field,
					std::forward< op_fn >( op ),
					std::make_index_sequence< sizeof ... (matcher_var) >{}
				);
			}

			std::string
			domain() const
			{
				return std::string( "(" ) + _domain_builder( std::make_index_sequence< sizeof ... (matcher_var) >{} );
			}

			template< typename r_matcher_fw >
			auto
			operator|( r_matcher_fw&& r ) const &
			{
				return or_t< matcher_var ..., std::decay_t< r_matcher_fw > >{ *this, std::forward< r_matcher_fw >( r ) };
			}

			template< typename r_matcher_fw >
			auto
			operator|( r_matcher_fw&& r ) &&
			{
				return or_t< matcher_var ..., std::decay_t< r_matcher_fw > >{ std::move( *this ), std::forward< r_matcher_fw >( r ) };
			}

		  private:


			template< size_t current_i, size_t ... i_var >
			std::string
			_domain_builder( std::index_sequence< current_i, i_var ... > ) const
			{
				return std::get< current_i >( matchers ).domain() + "|" + _domain_builder( std::index_sequence< i_var ... >{} );
			}

			template< size_t last_i >
			std::string
			_domain_builder( std::index_sequence< last_i > ) const
			{
				return std::get< last_i >( matchers ).domain() + _domain_builder( std::index_sequence< >{} );
			}

			std::string
			_domain_builder( std::index_sequence< > ) const
			{
				return ")";
			}

			template< typename op_fn, size_t current_i, size_t ... i_var >
			bool
			_visit( field_t field, op_fn && op, std::index_sequence< current_i, i_var ... > ) const
			{
				// Try to visit using the i'th matcher
				bool matched_i = std::get< current_i >( matchers ).visit(
					field,
					[&]( field_t unmatched, auto&& ... results )
					{
						return std::forward< op_fn >( op )( unmatched, std::forward< decltype( results ) >( results ) ... );
					}
				);

				if( matched_i )
				{
					return true;
				}
				else
				{
					// If we didn't find a match with the i'th matcher then try with the (i+1)'th ...
					return _visit(
						field,
						std::forward< op_fn >( op ),
						std::index_sequence< i_var ... >{}
					);
				}
			}

			template< size_t last_i, typename op_fn >
			bool
			_visit( field_t field, op_fn && op, std::index_sequence< last_i > ) const
			{
				return std::get< last_i >( matchers ).visit( field, std::forward< op_fn >( op ) );
			}

			std::tuple< matcher_var ... > matchers;

			// Make all or_t< ... > friends of this class so they have access to private members
			template< typename ... >
			friend class or_t;
		};

		template< typename delimit_matcher_t, typename ... matcher_var >
		class unordered_join_t : public matcher_base_t< unordered_join_t< delimit_matcher_t, matcher_var ... > >
		{
		  public:

			template< typename delimit_matcher_fw, typename ... matcher_fw_var >
			unordered_join_t( delimit_matcher_fw&& delimit_matcher, matcher_fw_var && ... matchers )
				: delimiter{ std::forward< delimit_matcher_fw >( delimit_matcher ) }
				, matchers{ std::forward< matcher_fw_var >( matchers ) ... }
			{}

			template< typename op_fn >
			bool
			visit( field_t field, op_fn && op ) const
			{
				return _visit(
					{ field },
					std::forward< op_fn >( op ),
					std::make_index_sequence< sizeof ... (matcher_var) >{}
				);
			}

			std::string
			domain() const
			{
				return std::string( "&(" ) + delimiter.domain() + ")[" + _domain_builder( std::make_index_sequence< sizeof ... (matcher_var) >{} );
			}

		  private:
			template< size_t current_i, size_t ... i_var >
			std::string
			_domain_builder( std::index_sequence< current_i, i_var ... > ) const
			{
				return std::get< current_i >( matchers ).domain() + ":" + _domain_builder( std::index_sequence< i_var ... >{} );
			}

			template< size_t last_i >
			std::string
			_domain_builder( std::index_sequence< last_i > ) const
			{
				return std::get< last_i >( matchers ).domain() + _domain_builder( std::index_sequence< >{} );
			}

			std::string
			_domain_builder( std::index_sequence< > ) const
			{
				return "]";
			}

			template< typename op_fn, size_t current_i, size_t ... i_var >
			bool
			_visit( std::vector< field_t > fields, op_fn && op, std::index_sequence< current_i, i_var ... > ) const
			{
				for( auto field_it = fields.begin(); field_it != fields.end(); ++field_it )
				{
					for( auto suffix_begin_it = field_it->begin();
					     suffix_begin_it <= field_it->end(); // NB: This is not `<` because want to try match the empty suffix as well
					     ++suffix_begin_it )
					{
						field_t pre_i{ field_it->begin(), suffix_begin_it };

						field_t to_be_matched_by_i{ suffix_begin_it, field_it->end() };

						bool matched_i_and_rest = std::get< current_i >( matchers ).visit(
							to_be_matched_by_i,
							[&]( field_t post_i, auto&& ... i_vals )
							{
								std::vector< field_t > fields_excluding_i;
								fields_excluding_i.insert( fields_excluding_i.end(), fields.begin(), field_it );

								if( post_i == to_be_matched_by_i )
								{
								    // If the matcher matched the empty field then we'll treat it as a special case that doesn't require a delimiter.
								    // This lets us support default values when a parameter is absent by having the matcher for that parameter operator| with a translation from the empty literal to the default value.
								    fields_excluding_i.emplace_back( *field_it );
								}
								else
								{
								    fields_excluding_i.emplace_back( pre_i );
								    fields_excluding_i.emplace_back( post_i );
								}

								fields_excluding_i.insert( fields_excluding_i.end(), std::next( field_it ), fields.end() );

								return _visit(
									fields_excluding_i,
									[&]( field_t unmatched, auto&& ... remaining_vals )
									{
										return std::forward< op_fn >( op )( unmatched, std::tuple_cat( std::forward_as_tuple( std::forward< decltype( i_vals ) >( i_vals ) ... ), std::forward< decltype( remaining_vals ) >( remaining_vals ) ... ) );
									},
									std::index_sequence< i_var ... >{}
								);
							}
						);

						if( matched_i_and_rest )
						{
							return true;
						}
					}
				}
				return false;
			}

			template< typename op_fn >
			bool
			_visit( std::vector< field_t > fields, op_fn && op, std::index_sequence< > ) const
			{
				if( fields.size() < 2u )
				{
					assert( false );
					return false;
				}
				if( !fields.begin()->empty() )
				{
					return false;
				}

				for( auto delimiter_field_it = std::next( fields.begin() ); delimiter_field_it != std::prev( fields.end() ); ++delimiter_field_it )
				{
					bool delimiter_fully_matched = delimiter.visit(
						*delimiter_field_it,
						[&]( field_t unmatched_by_delimiter, auto&& ... /* ignored delimiter vals :( */ )
						{
							return unmatched_by_delimiter.empty();
						}
					);
					if( !delimiter_fully_matched )
					{
						return false;
					}
				}
				return std::forward< op_fn >( op )( *fields.rbegin() );
			}

			delimit_matcher_t delimiter;
			std::tuple< matcher_var ... > matchers;
		};

		template< typename matcher_t >
		class override_domain_t : public matcher_base_t< override_domain_t< matcher_t > >
		{
		  public:
			template< typename matcher_fw >
			override_domain_t( const std::string& forced_domain, matcher_fw&& matcher )
				: forced_domain( forced_domain )
				, matcher( std::forward< matcher_fw >( matcher ) )
			{}

			template< typename op_fn >
			bool
			visit( field_t field, op_fn && op ) const
			{
				return matcher.visit( field, std::forward< op_fn >( op ) );
			}

			std::string
			domain() const
			{
				return forced_domain;
			}
		  private:
			std::string forced_domain;
			matcher_t matcher;
		};

		template< typename matcher_t >
		struct repeat_between_t : public matcher_base_t< repeat_between_t< matcher_t > >
		{
		  public:
			template< typename matcher_fw >
			repeat_between_t( size_t min, size_t max, matcher_fw && matcher )
				: min{ min }
				, max{ max }
				, matcher{ std::forward< matcher_fw >( matcher ) }
			{}

			template< typename op_fn >
			bool
			visit( field_t field, op_fn && op ) const
			{

				// The empty string is considered a match
				if( min == 0u )
				{
					if( std::forward< op_fn >( op )( field ) )
					{
						// early exit if the operation tells us to
						return true;
					}
				}
				std::list< std::pair< field_t, size_t > > unmatched_stack;
				unmatched_stack.emplace_back( field, 0u );


				while( !unmatched_stack.empty() )
				{
					field_t unmatched = std::move( unmatched_stack.back().first );
					size_t depth      = unmatched_stack.back().second;
					unmatched_stack.pop_back();

					if( depth + 1 > max )
					{
						continue;
					}

					bool matched = matcher.visit(
						unmatched,
						[&]( field_t child_unmatched, auto&& ... vals )
						{
							unmatched_stack.emplace_back( child_unmatched, depth + 1 );
							if( min <= depth + 1 )
							{
							    return std::forward< op_fn >( op )( child_unmatched, std::forward< decltype( vals ) >( vals ) ... );
							}
							else
							{
							    return false;
							}
						}
					);

					if( matched )
					{
						return true;
					}
				}
				return false;
			}

			std::string
			domain() const
			{
				std::stringstream ss;
				ss << matcher.domain() << "{" << min << "," << max << "}";
				return ss.str();
			}

		  private:

			size_t min;
			size_t max;
			matcher_t matcher;
		};

		template< typename matcher_t >
		class require_t : public matcher_base_t< require_t< matcher_t > >
		{
		  public:

			template< typename matcher_fw >
			require_t( matcher_fw&& matcher )
				: matcher( std::forward< matcher_fw >( matcher ) )
			{}

			template< typename op_fn >
			bool
			visit( field_t field, op_fn && op ) const
			{
				bool matched = matcher.visit(
					field,
					[&]( field_t unmatched, auto&& ... results )
					{
						return std::forward< op_fn >( op )( unmatched, std::forward< decltype( results ) >( results ) ... );
					}
				);

				if( !matched )
				{
					throw parse_exception( field.to_string(), matcher.domain(), "Failed to match a required input." );
				}

				return true;
			}

			std::string
			domain() const
			{
				return matcher.domain();
			}

		  private:

			matcher_t matcher;
		};

		template< typename matcher_t >
		class dquoted_t : public matcher_base_t< dquoted_t< matcher_t > >
		{
		  public:
			dquoted_t( matcher_t&& matcher )
				: matcher( std::move( matcher ) )
			{}

			dquoted_t( const matcher_t& matcher )
				: matcher( matcher )
			{}

			dquoted_t( const dquoted_t& ) = default;
			dquoted_t( dquoted_t&& )      = default;

			template< typename op_fn >
			bool
			visit( field_t field, op_fn && op ) const
			{
				if( field.begin() != field.end() && *field.begin() == '\"' )
				{
					bool is_escaping = false;
					std::stringstream unescaped_ss;
					for( auto it = std::next( field.begin() ); it != field.end(); ++it )
					{
						if( is_escaping )
						{
							// Strip out the '\' from occurrances of '\"'
							if( *it == '\"' )
							{
								unescaped_ss << *it;
							}
							else
							{
								unescaped_ss << "\\" << *it;
							}

							is_escaping = false;
						}
						else
						{
							// At the first occurrance of an unescaped '"', invoke the matcher and return
							if( *it == '\"' )
							{
								std::string unescaped_str = unescaped_ss.str();
								return matcher.visit(
									field_t{ unescaped_str },
									[&]( field_t unmatched, auto&& ... results )
									{
										if( unmatched.begin() == unescaped_str.end() )
										{
										    return std::forward< op_fn >( op )( field_t{ std::next( it ), field.end() }, std::forward< decltype( results ) >( results ) ... );
										}
										return false;
									}
								);
							}
							else if( *it == '\\' )
							{
								is_escaping = true;
							}
							else
							{
								unescaped_ss << *it;
							}
						}
					}
				}
				return false;
			}

			std::string
			domain() const
			{
				std::stringstream ss;
				ss << "(\"" << matcher.domain() << "\"" << "&&" << "^(\"(.|\\n)*\"(.|\\n)*\"))";
				return ss.str();
			}

		  private:
			matcher_t matcher;
		};

		template< typename val_t, typename Enable = void >
		class param_t;

		template< typename val_t >
		class param_t< val_t, std::enable_if_t< std::is_unsigned_v< val_t > > > : public matcher_base_t< param_t< val_t > >
		{
		  public:

			param_t()
			{}

			template< typename op_fn >
			bool
			visit( field_t field, op_fn && op ) const
			{
				if( field.size() > 0 && !std::isspace( *field.begin() ) && *field.begin() != '-' )
				{
					const char* str_begin = &( *field.begin() );
					char *str_end;
					unsigned long long val = std::strtoull( str_begin, &str_end, 10 );
					if( errno == ERANGE )
					{
						errno = 0;
					}
					else if( str_begin != str_end && std::numeric_limits< val_t >::min() <= val && val <= std::numeric_limits< val_t >::max() )
					{
						return std::forward< op_fn >( op )( field_t{ std::next( field.begin(), str_end - str_begin ), field.end() }, static_cast< val_t >( val ) );
					}
				}
				return false;
			}

			std::string
			domain() const
			{
				return "\\d+";
			}
		};

		template< typename val_t >
		class param_t< val_t, std::enable_if_t< std::is_signed_v< val_t > > > : public matcher_base_t< param_t< val_t > >
		{
		  public:

			param_t()
			{}

			template< typename op_fn >
			bool
			visit( field_t field, op_fn && op ) const
			{
				if( field.size() > 0 && !std::isspace( *field.begin() ) )
				{
					const char* str_begin = &( *field.begin() );
					char *str_end;
					long long val = std::strtoll( str_begin, &str_end, 10 );
					if( errno == ERANGE )
					{
						errno = 0;
					}
					else if( str_begin != str_end && std::numeric_limits< val_t >::min() <= val && val <= std::numeric_limits< val_t >::max() )
					{
						return std::forward< op_fn >( op )( field_t{ std::next( field.begin(), str_end - str_begin ), field.end() }, static_cast< val_t >( val ) );
					}
				}
				return false;
			}

			std::string
			domain() const
			{
				return "(\\+|-)?\\d+";
			}
		};

		template< >
		class param_t< std::string > : public matcher_base_t< param_t< std::string > >
		{
		  public:

			param_t()
			{}

			template< typename op_fn >
			bool
			visit( field_t field, op_fn && op ) const
			{
				for( auto it = field.begin(); it <= field.end(); ++it )
				{
					if( std::forward< op_fn >( op )( field_t{ it, field.end() }, std::string( field.begin(), it ) ) )
					{
						return true;
					}
				}
				return false;
			}

			std::string
			domain() const
			{
				return "(.|\\n)*";
			}
		};

		template< typename matcher_t, typename val_t >
		class translate_t : public matcher_base_t< translate_t< matcher_t, val_t > >
		{
		  public:
			template< typename matcher_fw, typename val_fw >
			translate_t( matcher_fw&& matcher, val_fw&& val )
				: matcher{ std::forward< matcher_fw >( matcher ) }
				, val{ std::forward< val_fw >( val ) }
			{}

			template< typename op_fn >
			bool
			visit( field_t field, op_fn && op ) const
			{
				return matcher.visit(
					field,
					[&]( field_t unmatched, auto&& ... results )
					{
						return std::forward< op_fn >( op )( unmatched, std::forward< decltype( results ) >( results ) ..., val );
					}
				);
			}

			std::string
			domain() const
			{
				return matcher.domain();
			}

		  private:
			matcher_t matcher;
			val_t val;
		};
	}



	template< typename delimiter_fw, typename ... matcher_fw_var >
	auto
	delimited_unordered_join( delimiter_fw&& delimiter, matcher_fw_var&& ... matchers )
	{
		return detail::unordered_join_t< std::decay_t< delimiter_fw >, std::decay_t< matcher_fw_var > ... >{ std::forward< delimiter_fw >( delimiter ), std::forward< matcher_fw_var >( matchers ) ... };
	}

	template< typename matcher_t >
	const detail::matcher_base_t< std::decay_t< matcher_t > >&
	atomic( const matcher_t& matcher )
	{
		return static_cast< const detail::matcher_base_t< std::decay_t< matcher_t > >& >( matcher );
	}
	template< typename matcher_t >
	detail::matcher_base_t< std::decay_t< matcher_t > >&&
	atomic( matcher_t && matcher )
	{
		return static_cast< detail::matcher_base_t< std::decay_t< matcher_t > >&& >( matcher );
	}


	template< typename matcher_fw >
	auto
	override_domain( const std::string& forced_domain, matcher_fw&& matcher )
	{
		return detail::override_domain_t< std::decay_t< matcher_fw > >{ forced_domain, std::forward< matcher_fw >( matcher ) };
	}

	template< typename matcher_fw >
	auto
	repeat_between( size_t min, size_t max, matcher_fw&& matcher )
	{
		return detail::repeat_between_t< std::decay_t< matcher_fw > >{ min, max, std::forward< matcher_fw >( matcher ) };
	}

	template< typename matcher_fw >
	auto
	repeat_up_to( size_t n, matcher_fw&& matcher )
	{
		return repeat_between( 0u, n, std::forward< matcher_fw >( matcher ) );
	}

	template< typename matcher_fw >
	auto
	maybe( matcher_fw&& matcher )
	{
		std::string orig_domain = matcher.domain();
		return override_domain( orig_domain + "?", repeat_between( 0u, 1u, std::forward< matcher_fw >( matcher ) ) );
	}

	template< typename matcher_fw >
	auto
	repeat_exactly( size_t n, matcher_fw&& matcher )
	{
		std::string orig_domain = matcher.domain();
		std::stringstream forced_domain;
		forced_domain << orig_domain << "{" << n << "}";
		return override_domain( forced_domain.str(), repeat_between( n, n, std::forward< matcher_fw >( matcher ) ) );
	}

	template< typename matcher_fw >
	auto
	repeat_any( matcher_fw&& matcher )
	{
		std::string orig_domain = matcher.domain();
		return override_domain( orig_domain + "*", repeat_up_to( std::numeric_limits< size_t >::max(), std::forward< matcher_fw >( matcher ) ) );
	}

	template< typename matcher_fw >
	auto
	repeat_at_least_once( matcher_fw&& matcher )
	{
		std::string orig_domain = matcher.domain();
		return override_domain( orig_domain + "+", repeat_between( 1u, std::numeric_limits< size_t >::max(), std::forward< matcher_fw >( matcher ) ) );
	}

	template< typename matcher_fw >
	auto
	require( matcher_fw&& matcher )
	{
		return detail::require_t< std::decay_t< matcher_fw > >{ std::forward< matcher_fw >( matcher ) };
	}

	template< typename matcher_fw >
	auto
	dquoted( matcher_fw&& matcher )
	{
		return detail::dquoted_t< std::decay_t< matcher_fw > >( std::forward< matcher_fw >( matcher ) );
	}

	template< typename val_t >
	auto
	param()
	{
		return detail::param_t< val_t >{};
	}

	template< typename matcher_fw, typename val_fw >
	auto
	translate( matcher_fw&& matcher, val_fw&& val )
	{
		return detail::translate_t< std::decay_t< matcher_fw >, std::decay_t< val_fw > >{ std::forward< matcher_fw >( matcher ), std::forward< val_fw >( val )  };
	}
}