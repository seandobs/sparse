///
/// @brief A `command_line_args_t` is struct for parsing command line args in the typical unix format.
///
/// @file command_line_args_t.h
/// @author Sean Dobson, seandobs@gmail.com
/// @date 2019-02-20
///

#pragma once

#include <string>
#include <list>
#include <optional>

namespace sparse
{
	/// TODO: docs
	class cmdline_args_t
	{
	  public:
		cmdline_args_t( int argc, char** argv );
		cmdline_args_t( std::string executable_name, std::list< std::string > args );

		static
		std::string
		get_domain_for_param( const std::string& longform, const char shortform );

		std::optional< std::string >
		pop_param( const std::string& longform, const char shortform = '\0' );

		std::string
		pop_required_param( const std::string& longform, const char shortform = '\0' );

		bool
		pop_flag( const std::string& longform, const char shortform = '\0' );

		void
		finalize() const;
	  private:
		std::string executable_name;
		std::list< std::string > args;
	};
} // namespace sparse
