///
/// @brief A `field_t` is essentially two iterators defining a contiguous range of chars in a std::string
///  Its purpose is to allow passing around such a range without creating new strings, or having to pass both iterators individually.
///
/// @file field_t.h
/// @author Sean Dobson, seandobs@gmail.com
/// @date 2019-02-20
///

#pragma once

#include <string>

namespace sparse
{
	/// TODO: docs
	class field_t
	{
	  public:

		using const_iterator         = std::string::const_iterator;
		using const_reverse_iterator = std::string::const_reverse_iterator;

		field_t( const_iterator begin, const_iterator end );

		field_t( const std::string& str );

		field_t( const std::string&& ) = delete;

		std::string to_string() const;

		const_iterator begin() const;

		const_iterator end() const;

		const_reverse_iterator rbegin() const;

		const_reverse_iterator rend() const;

		size_t size() const;
		bool empty() const;

		bool
		operator==( const field_t& other ) const;

		bool
		operator!=( const field_t& other ) const;
	  private:
		const_iterator begin_it;
		const_iterator end_it;
	};
} // namespace sparse
