///
/// @brief A library of composable string parsing matchers which implement the visitor pattern.
///
/// @file matchers.h
/// @author Sean Dobson, seandobs@gmail.com
/// @date 2018-04-18
///

#pragma once

#include <string>
#include <sstream>
#include <tuple>
#include <utility>
#include <limits>
#include <cctype>
#include <list>
#include <vector>

#include <cassert>
#include <cerrno>
#include <cstdlib>

#include "field_t.h"
#include "parse_exception.h"

#include "dynamic_matchers.h"
#include "static_matchers.h"

namespace sparse
{
	template< typename matcher_fw >
	auto
	assignment( std::string name, matcher_fw&& matcher )
	{
		return literal( std::move( name ) ) + ws() + literal( "=" ) + ws() + std::forward< matcher_fw >( matcher );
	}

	template< typename first_matcher_fw, typename ... matcher_fw_var >
	auto
	sequence( first_matcher_fw&& first, matcher_fw_var&& ... matchers )
	{
		return std::forward< first_matcher_fw >( first ) + ( ( ws() + literal( "," ) + ws() + std::forward< matcher_fw_var >( matchers ) ) + ... );
	}

	inline
	auto
	sequence()
	{
		return literal( "" );
	}

	template< typename ... matcher_fw_var >
	auto
	unordered_join( matcher_fw_var&& ... matchers )
	{
		return detail::unordered_join_t< dynamic::matcher_t, std::decay_t< matcher_fw_var > ... >{ literal( "" ), std::forward< matcher_fw_var >( matchers ) ... };
	}

	template< typename matcher_t >
	struct arg_t
	{
		std::string arg_name;
		matcher_t arg_matcher;
	};

	template< typename matcher_t >
	auto
	arg( std::string arg_name, matcher_t arg_matcher )
	{
		return arg_t< matcher_t >{ std::move( arg_name ), std::move( arg_matcher ) };
	}

	template< typename matcher_t, typename val_t >
	struct defaulted_arg_t
	{
		std::string arg_name;
		matcher_t arg_matcher;
		val_t default_val;
	};

	template< typename matcher_t, typename val_t >
	auto
	defaulted_arg( std::string arg_name, matcher_t arg_matcher, val_t default_val )
	{
		return defaulted_arg_t< matcher_t, val_t >{ std::move( arg_name ), std::move( arg_matcher ), std::move( default_val ) };
	}

	namespace detail
	{

		template< typename first_matcher_t, typename ... matcher_var, typename ... defaulted_matcher_var, typename ... matcher_defaults_var, size_t ... matcher_var_i, size_t ... defaulted_matcher_var_i >
		auto
		_function( std::string name, std::tuple< arg_t< first_matcher_t >, arg_t< matcher_var > ... > matchers, std::tuple< defaulted_arg_t< defaulted_matcher_var, matcher_defaults_var > ... > defaulted_matchers, std::index_sequence< matcher_var_i ... >, std::index_sequence< defaulted_matcher_var_i ... > )
		{
			// delimit args on commas, allowing for arbitary whitespace
			auto delimiter = ws() + literal( "," ) + ws();

			// matches the in-order unnamed parameter syntax
			auto sequential_arg_matcher = std::get< 0 >( matchers ).arg_matcher + ( ( delimiter + std::get< 1u + matcher_var_i >( matchers ).arg_matcher ) + ... ) + ( ( delimiter + std::get< defaulted_matcher_var_i >( defaulted_matchers ).arg_matcher ) + ... );

			// matches the unordered named parameter syntax
			auto named_arg_matcher = delimited_unordered_join(
				std::move( delimiter ),
				assignment( std::move( std::get< 0 >( matchers ).arg_name ), std::move( std::get< 0 >( matchers ).arg_matcher ) ),
				assignment( std::move( std::get< 1u + matcher_var_i >( matchers ).arg_name ), std::move( std::get< 1u + matcher_var_i >( matchers ).arg_matcher ) ) ...,
				( assignment( std::move( std::get< defaulted_matcher_var_i >( defaulted_matchers ).arg_name ), std::move( std::get< defaulted_matcher_var_i >( defaulted_matchers ).arg_matcher ) )
				  | translate( literal( "" ), std::move( std::get< defaulted_matcher_var_i >( defaulted_matchers ).default_val ) ) ) ...
			);

			return
			    join( literal( std::move( name ) ) + ws() + literal( "(" ) + ws(),
			          std::move( sequential_arg_matcher ) | std::move( named_arg_matcher ),
			          ws() + literal( ")" ) );
		}
	}

	template< typename first_matcher_t, typename ... matcher_var, typename ... defaulted_matcher_var, typename ... matcher_defaults_var >
	auto
	function( std::string name, std::tuple< arg_t< first_matcher_t >, arg_t< matcher_var > ... > matchers, std::tuple< defaulted_arg_t< defaulted_matcher_var, matcher_defaults_var > ... > defaulted_matchers )
	{
		return detail::_function( std::move( name ), std::move( matchers ), std::move( defaulted_matchers ), std::index_sequence_for< matcher_var ... >(), std::index_sequence_for< defaulted_matcher_var ... >() );
	}
}