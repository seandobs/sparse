///
/// @brief A library of composable string parsing matchers which implement the visitor pattern.
///
/// @file matchers.h
/// @author Sean Dobson, seandobs@gmail.com
/// @date 2018-04-18
///

#pragma once

#include <string>
#include <sstream>
#include <tuple>
#include <utility>
#include <limits>
#include <cctype>
#include <list>
#include <vector>

#include <functional>
#include <memory>

#include <cassert>
#include <cerrno>
#include <cstdlib>

#include "field_t.h"
#include "parse_exception.h"

#include "static_matchers.h"

namespace sparse
{
	namespace dynamic
	{
		struct matcher_i
		{
			virtual
			bool
			visit( field_t field, const std::function< bool(field_t) >& op ) const = 0;

			virtual
			std::string
			domain() const = 0;

			virtual
			std::unique_ptr< matcher_i >
			clone() const = 0;
		};

		class matcher_t : public sparse::detail::matcher_base_t< matcher_t >
		{
		  public:
			matcher_t( std::unique_ptr< matcher_i > impl );

			matcher_t( const matcher_t& other );

			matcher_t( matcher_t&& other );

			matcher_t&
			operator=( const matcher_t& other );

			matcher_t&
			operator=( matcher_t&& other );

			bool
			visit( field_t field, const std::function< bool(field_t) >& op ) const;

			std::string
			domain() const;

			matcher_t
			operator+( matcher_t other ) const;

			template< typename matcher_fw >
			auto
			operator+( matcher_fw&& other ) const
			{
				return sparse::detail::matcher_base_t< matcher_t >::operator+( std::forward< matcher_fw >( other ) );
			}

			matcher_t
			operator|( matcher_t other ) const;

			template< typename matcher_fw >
			auto
			operator|( matcher_fw&& other ) const
			{
				return sparse::detail::matcher_base_t< matcher_t >::operator|( std::forward< matcher_fw >( other ) );
			}

			matcher_i*
			get() const
			{
				return impl.get();
			}

		  private:

			std::unique_ptr< matcher_i > impl;
		};

		class literal_t : public matcher_i
		{
		  public:
			literal_t( std::string val )
				: val( std::move( val ) )
			{}

			virtual
			bool
			visit( field_t field, const std::function< bool(field_t) >& op ) const final;

			virtual
			std::string
			domain() const final;

			virtual
			std::unique_ptr< matcher_i >
			clone() const final;

		  private:
			std::string val;
		};

		class wild_t : public matcher_i
		{
		  public:
			wild_t();

			virtual
			bool
			visit( field_t field, const std::function< bool(field_t) >& op ) const final;

			virtual
			std::string
			domain() const final;

			virtual
			std::unique_ptr< matcher_i >
			clone() const final;
		};

		class end_of_field_t : public matcher_i
		{
		  public:
			end_of_field_t();

			virtual
			bool
			visit( field_t field, const std::function< bool(field_t) >& op ) const final;

			virtual
			std::string
			domain() const final;

			virtual
			std::unique_ptr< matcher_i >
			clone() const final;
		};

		class join_t : public matcher_i
		{
		  public:
			join_t( std::vector< matcher_t > matchers );

			virtual
			bool
			visit( field_t field, const std::function< bool(field_t) >& op ) const final;

			virtual
			std::string
			domain() const final;

			virtual
			std::unique_ptr< matcher_i >
			clone() const final;

			join_t
			operator+( matcher_t other ) const;

		  private:
			bool
			_visit( field_t field, const std::function< bool(field_t) >& op, size_t current_matcher_i ) const;

			std::vector< matcher_t > matchers;
		};

		class or_t : public matcher_i
		{
		  public:
			or_t( std::vector< matcher_t > matchers );

			virtual
			bool
			visit( field_t field, const std::function< bool(field_t) >& op ) const final;

			virtual
			std::string
			domain() const final;

			virtual
			std::unique_ptr< matcher_i >
			clone() const final;

			or_t
			operator|( matcher_t other ) const;

		  private:

			std::vector< matcher_t > matchers;
		};

		class unordered_join_t : public matcher_i
		{
		  public:
			unordered_join_t( matcher_t delimiter, std::vector< matcher_t > matchers );

			virtual
			bool
			visit( field_t field, const std::function< bool(field_t) >& op ) const final;

			virtual
			std::string
			domain() const final;

			virtual
			std::unique_ptr< matcher_i >
			clone() const final;

		  private:

			bool
			_visit( field_t field, const std::function< bool(field_t) >& op, std::vector< size_t > unmatched_indices ) const;

			matcher_t delimiter;
			std::vector< matcher_t > matchers;
		};

		class override_domain_t : public matcher_i
		{
		  public:
			override_domain_t( const std::string& forced_domain, matcher_t matcher );

			virtual
			bool
			visit( field_t field, const std::function< bool(field_t) >& op ) const final;

			virtual
			std::string
			domain() const final;

			virtual
			std::unique_ptr< matcher_i >
			clone() const final;

		  private:
			std::string forced_domain;
			matcher_t matcher;
		};

		struct repeat_between_t : public matcher_i
		{
		  public:
			repeat_between_t( size_t min, size_t max, matcher_t matcher );

			virtual
			bool
			visit( field_t field, const std::function< bool(field_t) >& op ) const final;

			virtual
			std::string
			domain() const final;

			virtual
			std::unique_ptr< matcher_i >
			clone() const final;

		  private:

			size_t min;
			size_t max;
			matcher_t matcher;
		};
	}

	dynamic::matcher_t
	literal( std::string val );

	template< typename ... matcher_var >
	dynamic::matcher_t
	delimited_unordered_join( dynamic::matcher_t delimiter, matcher_var ... matchers, typename std::enable_if< std::is_convertible_v< matcher_var, dynamic::matcher_t > ... >* = nullptr )
	{
		return dynamic::matcher_t{ std::unique_ptr< dynamic::matcher_i >( new dynamic::unordered_join_t{ std::move( delimiter ), { std::move( matchers ) ... } } ) };
	}

	template< typename ... matcher_var >
	dynamic::matcher_t
	unordered_join( matcher_var ... matchers, typename std::enable_if< std::is_convertible_v< matcher_var, dynamic::matcher_t > ... >* = nullptr )
	{
		return dynamic::matcher_t{ std::unique_ptr< dynamic::matcher_i >( new dynamic::unordered_join_t{ literal( "" ), { std::move( matchers ) ... } } ) };
	}

	dynamic::matcher_t
	wild();

	///
	/// @brief Produces a matcher which matches the end of the inputted field.
	///
	dynamic::matcher_t
	end_of_field();

	dynamic::matcher_t
	override_domain( const std::string& forced_domain, dynamic::matcher_t matcher );

	dynamic::matcher_t
	repeat_between( size_t min, size_t max, dynamic::matcher_t matcher );

	dynamic::matcher_t
	repeat_up_to( size_t n, dynamic::matcher_t matcher );

	dynamic::matcher_t
	maybe( dynamic::matcher_t matcher );

	dynamic::matcher_t
	repeat_exactly( size_t n, dynamic::matcher_t matcher );

	dynamic::matcher_t
	repeat_any( dynamic::matcher_t matcher );

	dynamic::matcher_t
	repeat_at_least_once( dynamic::matcher_t matcher );

	dynamic::matcher_t
	whitespaces();

	dynamic::matcher_t
	ws();
}