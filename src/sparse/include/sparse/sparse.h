///
/// @brief A quick and dirty parsing library
///
/// @file sparse.h
/// @author Sean Dobson, seandobs@gmail.com
/// @date 2018-04-18
///

#pragma once

#include <string>
#include <sstream>
#include <utility>
#include <optional>
#include <cctype>
#include <functional>
#include <algorithm>
#include <set>
#include <map>
#include <vector>
#include <list>
#include <memory>

#include <errno.h>
#include <cstdlib>

#include "field_t.h"
#include "parse_exception.h"

namespace sparse
{
	static const std::set< std::string > whitespace = { " ", "\t", "\n", "\v", "\f", "\r" };

	static const std::pair< char, char > paren_delims( '(', ')' );
	static const std::pair< char, char > square_brace_delims( '[', ']' );
	static const std::pair< char, char > curly_brace_delims( '{', '}' );
	static const std::pair< char, char > angle_brace_delims( '<', '>' );
	static const std::pair< char, char > single_quote_delims( '\'', '\'' );
	static const std::pair< char, char > double_quote_delims( '"', '"' );

	static const std::set< std::pair< char, char > > all_block_delims = {
		paren_delims,
		square_brace_delims,
		curly_brace_delims,
		angle_brace_delims,
		single_quote_delims,
		double_quote_delims
	};

	template< typename Iterator >
	Iterator skip_delim( Iterator begin, Iterator end, Iterator delim_begin, Iterator delim_end );

	std::string::const_iterator skip_block( std::string::const_iterator begin, std::string::const_iterator end, std::pair< char, char > block_delim );

	std::pair< field_t, field_t >
	split( field_t field, const std::string& delimiter, const std::set< std::pair< char, char > >& block_delims, bool purge_delim = true );

	std::vector< field_t >
	tokenize( field_t field, const std::string& delimiter, const std::set< std::pair< char, char > >& block_delims );

	std::vector< field_t >
	tokenize( field_t field, const std::set< std::string >& delimiters, const std::set< std::pair< char, char > >& block_delims );

	std::string
	join( const std::vector< field_t >& fields, const std::string& separator = "" );

	std::string
	join_non_empty( const std::vector< field_t >& fields, const std::string& separator = "" );

	std::string
	strip( field_t field, const std::set< std::string >& delimiters, const std::set< std::pair< char, char > >& block_delims );

	std::string
	strip_whitespace( field_t field, const std::set< std::pair< char, char > >& block_delims = { {'"', '"'} } );

	std::string
	squeeze( field_t field, const std::set< std::string >& delimiters, const std::set< std::pair< char, char > >& block_delims, const std::string& separator = "" );

	std::string
	squeeze_whitespace( field_t field, const std::set< std::pair< char, char > >& block_delims = { {'"', '"'} } );

	field_t
	trim( field_t field, const std::set< std::string >& delimiters );

	field_t
	trim_whitespace( field_t field );

	field_t
	trim_block( field_t field, const std::pair< char, char >& block_delim );

	bool
	is_varname( field_t field );

	bool
	is_block( field_t field, const std::pair< char, char >& block_delim );

	std::pair< field_t, field_t >
	assignment_split( field_t field, const std::set< std::pair< char, char > >& block_delims = all_block_delims );

	template< typename T >
	class parser_t
	{
	  public:
		virtual T parse( field_t field ) const = 0;
		virtual std::string
		get_domain() const
		{
			return "ANY";
		}
	};

	template< typename T >
	class option_map_parser : public parser_t< T >
	{
	  public:
		option_map_parser( const std::map< std::string, T >& option_map )
			: option_map( option_map )
		{}

		virtual T
		parse( field_t field ) const override
		{
			std::string option = trim_block( trim_whitespace( field ), { '"', '"' } ).to_string();

			if( option_map.count( option ) == 0 )
			{
				throw parse_exception( field.to_string(), get_domain(), "The option \"" + option + "\" was not recognized." );
			}
			else
			{
				return option_map.at( option );
			}
		}

		virtual
		std::string
		get_domain() const override
		{
			std::vector< field_t > options;
			for( const auto& option : option_map )
			{
				options.emplace_back( option.first );
			}

			return "{" + join( options, "," ) + "}";
		}

	  private:

		std::map< std::string, T > option_map;
	};

	template< typename T >
	class default_parser_t
	{};

	template< >
	class default_parser_t< unsigned int > : public parser_t< unsigned int >
	{
	  public:
		virtual unsigned int
		parse( field_t field ) const override
		{
			try
			{
				return stoul( field.to_string() );
			}
			catch( const std::invalid_argument& e )
			{
				throw parse_exception( field.to_string(), get_domain(), "Not a non-negative integer." );
			}
		}
		virtual std::string
		get_domain() const override
		{
			return "NON_NEGATIVE_INTEGER";
		}
	};

	template< >
	class default_parser_t< int > : public parser_t< int >
	{
	  public:
		virtual int
		parse( field_t field ) const override
		{
			try
			{
				return stol( field.to_string() );
			}
			catch( const std::invalid_argument& e )
			{
				throw parse_exception( field.to_string(), get_domain(), "Not an integer." );
			}
		}
		virtual std::string
		get_domain() const override
		{
			return "INTEGER";
		}
	};

	template< >
	class default_parser_t< double > : public parser_t< double >
	{
	  public:
		virtual double
		parse( field_t field ) const override
		{
			try
			{
				return stod( field.to_string() );
			}
			catch( const std::invalid_argument& e )
			{
				throw parse_exception( field.to_string(), get_domain(), "Not a real number." );
			}
		}
		virtual std::string
		get_domain() const override
		{
			return "REAL";
		}
	};

	template< >
	class default_parser_t< std::string > : public parser_t< std::string >
	{
	  public:
		virtual std::string
		parse( field_t field ) const override
		{
			return trim_block( trim_whitespace( field ), { '"', '"' } ).to_string();
		}
		virtual std::string
		get_domain() const override
		{
			return "STRING";
		}
	};

	template< >
	class default_parser_t< bool > : public option_map_parser< bool >
	{
	  public:
		default_parser_t()
			: option_map_parser(
				{
				    { "true", true }, { "false", false }
				} )
		{}
	};

	template< typename T >
	class constrained_parser_t : public parser_t< T >
	{
	  public:

		struct constraint_t
		{
			virtual bool operator()( const T& value ) = 0;

			virtual std::string describe_constraint( const std::string& domain_member ) = 0;
		};

		constrained_parser_t( const std::shared_ptr< parser_t< T > >& parser, const std::shared_ptr< constraint_t >& constraint )
			: parser( parser )
			, constraint( constraint )
		{}

		virtual T
		parse( field_t field ) const override
		{
			try
			{
				T value = parser->parse( field );

				if( !constraint->operator()( value ) )
				{
					std::string field_str = trim_whitespace( field ).to_string();
					throw parse_exception( field_str, get_domain(), "Violated the constraint: " + constraint->describe_constraint( field_str ) );
				}

				return value;
			}
			catch( const parse_exception& e )
			{
				if( e.get_domain() != get_domain() )
				{
					throw parse_exception( field.to_string(), get_domain(), e.what() );
				}
				else
				{
					throw e;
				}
			}


		}
		virtual std::string
		get_domain() const override
		{
			std::string domain_member = "x";
			return "{"
			       + domain_member + "∈" + parser->get_domain()
			       + "|" + constraint->describe_constraint( domain_member )
			       + "}";
		}

	  private:
		std::shared_ptr< parser_t< T > > parser;
		std::shared_ptr< constraint_t > constraint;
	};

	template< typename T >
	class range_constrained_parser_t : public constrained_parser_t< T >
	{

		using base = constrained_parser_t< T >;

		class range_constraint_t : public base::constraint_t
		{
		  public:
			range_constraint_t( const T& min_val, const T& max_val, const std::string& min_str, const std::string& max_str )
				: min_val( min_val )
				, max_val( max_val )
				, min_str( min_str )
				, max_str( max_str )
			{}


			virtual bool
			operator()( const T& val ) override
			{
				return min_val <= val && val <= max_val;
			}

			virtual std::string
			describe_constraint( const std::string& domain_member ) override
			{
				return min_str + "≤" + domain_member + "≤" + max_str;
			}

		  private:
			T min_val;
			T max_val;
			std::string min_str;
			std::string max_str;
		};


	  public:

		range_constrained_parser_t( const std::shared_ptr< parser_t< T > >& parser, const std::string& min_str, const std::string& max_str )
			: base( parser,
			        std::make_shared< range_constraint_t >(
						parser->parse( min_str ), parser->parse( max_str ),
						min_str, max_str
			        )
			)
		{}
	};

	template< typename T >
	struct assignment_config
	{
		assignment_config( const std::string& varname, std::shared_ptr< parser_t< T > > parser )
			: varname( varname )
			, parser( parser )
			, has_default( false )
		{}

		assignment_config( const std::string& varname, std::shared_ptr< parser_t< T > > parser, const std::string& default_str )
			: varname( varname )
			, parser( parser )
			, default_str( default_str )
			, has_default( true )
		{}

		std::string varname;
		std::shared_ptr< parser_t< T > > parser;
		std::string default_str;
		bool has_default;
	};

	template< typename ... Args >
	class assignment_list_parser_t : parser_t< std::tuple< Args ... > >
	{
		template< size_t I >
		using ith_arg_t = typename std::tuple_element< I, std::tuple< Args ... > >::type;

	  public:

		assignment_list_parser_t( const assignment_config< Args >& ... configs )
			: configs( configs ... )
		{}


		template< size_t I >
		std::pair< ith_arg_t< I >, std::vector< field_t > >
		parse_ith_arg( field_t full_field, const std::vector< field_t >& fields ) const
		{
			const assignment_config< ith_arg_t< I > >& config = std::get< I >( configs );

			std::vector< field_t > matched_fields;
			std::vector< field_t > unmatched_fields;

			for( const field_t& field : fields )
			{
				std::pair< field_t, field_t > assignment = assignment_split( field );

				if( !is_varname( assignment.first ) )
				{
					throw parse_exception( field.to_string(), get_domain(), "\"" + assignment.first.to_string() + "\" is not in the form of a variable name" );
				}

				if( assignment.first.to_string() == config.varname )
				{
					matched_fields.push_back( assignment.second );
				}
				else
				{
					unmatched_fields.push_back( field );
				}
			}

			if( matched_fields.size() == 0 )
			{
				if( config.has_default )
				{
					try
					{
						return std::pair< ith_arg_t< I >, std::vector< field_t > >(
							config.parser->parse( config.default_str ),
							unmatched_fields
						);
					}
					catch( const parse_exception& e )
					{
						throw parse_exception( config.varname + "=" + config.default_str, get_ith_domain< I >(), e.what() );
					}

				}
				else
				{
					throw parse_exception( full_field.to_string(), get_domain(), "0 matches for parameter \"" + config.varname + "\"." );
				}
			}
			else if( matched_fields.size() > 1 )
			{
				throw parse_exception( full_field.to_string(), get_domain(), "Multiple matches for parameter \"" + config.varname + "\"." );
			}

			try
			{
				return std::pair< ith_arg_t< I >, std::vector< field_t > >(
					config.parser->parse( *matched_fields.begin() ),
					unmatched_fields
				);
			}
			catch( const parse_exception& e )
			{
				throw parse_exception( config.varname + "=" + matched_fields.begin()->to_string(), get_ith_domain< I >(), e.what() );
			}

		}

		std::tuple< >
		recursive_parse( field_t full_field, const std::vector< field_t >& fields, std::index_sequence< > ) const
		{
			if( !fields.empty() )
			{
				throw parse_exception( full_field.to_string(), get_domain(), "Could not match fields: " + join( fields, ", " ) );
			}

			return std::tuple< >();
		}

		template< std::size_t I, std::size_t ... Rest >
		std::tuple< ith_arg_t< I >, ith_arg_t< Rest >... >
		recursive_parse( field_t full_field, const std::vector< field_t >& fields, std::index_sequence< I, Rest ... >  ) const
		{
			auto parsed_arg = parse_ith_arg< I >( full_field, fields );

			//  /\       (
			// (' )_____  )
			//  ( x,y,z )/
			//  ||     ||

			return std::tuple_cat(
				std::tuple< ith_arg_t< I > >( parsed_arg.first ),
				recursive_parse( full_field, parsed_arg.second, std::index_sequence< Rest ... >() )
			);
		}

		virtual std::tuple< Args ... >
		parse( field_t field ) const override
		{
			std::vector< field_t > fields = tokenize( field, ",", all_block_delims );

			return recursive_parse( field, fields, std::index_sequence_for< Args ... >() );
		}


		template< std::size_t I >
		std::string
		get_ith_domain() const
		{
			const assignment_config< ith_arg_t< I > >& config = std::get< I >( configs );

			return config.varname + "="
			       + "["
			       + config.parser->get_domain()
			       + ( config.has_default ? ",default=" + config.default_str : "" )
			       + "]";
		}


		std::string
		recursive_get_domain( std::index_sequence< > ) const
		{
			return "";
		}

		template< std::size_t I, std::size_t ... Rest >
		std::string
		recursive_get_domain( std::index_sequence< I, Rest ... >  ) const
		{
			std::string ith_domain  = get_ith_domain< I >();
			std::string rest_domain = recursive_get_domain( std::index_sequence< Rest ... >() );

			return join_non_empty( { field_t( ith_domain ), field_t( rest_domain ) }, ", " );
		}

		virtual
		std::string
		get_domain() const override
		{
			return "( " + recursive_get_domain( std::index_sequence_for< Args ... >() ) + " )";
		}

	  private:

		std::tuple< assignment_config< Args >... > configs;
	};


	template< typename T >
	class fn_parser_t : public parser_t< T >
	{
	  public:
		template< typename ... Args >
		fn_parser_t( const std::function< T( Args ... ) >& fn, const std::string& fn_name, const assignment_config< Args >& ... configs )
			: fn_name( fn_name )
		{
			if( !is_varname( fn_name ) )
			{
				throw std::runtime_error( "Tried to create a function parser for \"" + fn_name + "\" which is not in the form of a function name." );
			}

			assignment_list_parser_t< Args ... > arg_parser( configs ... );
			arg_domains = arg_parser.get_domain();

			parser_fn =
				[ = ]( const field_t& field )
				{
					return std::apply( fn, arg_parser.parse( field ) );
				};
		}

		virtual T
		parse( field_t field ) const override
		{
			std::pair< field_t, field_t > fn_split = split( field, "(", all_block_delims, false );

			field_t fn_name  = trim_whitespace( fn_split.first );
			field_t arg_list = trim_whitespace( fn_split.second );

			if( fn_name.to_string() != this->fn_name )
			{
				throw parse_exception( field.to_string(), get_domain(), "The function name did not match." );
			}

			if( !is_block( arg_list, paren_delims ) )
			{
				throw parse_exception( field.to_string(), get_domain(), "The function arguments were not enclosed in parentheses." );
			}

			try
			{
				return parser_fn( trim_block( arg_list, paren_delims ) );
			}
			catch( const parse_exception& e )
			{
				throw parse_exception( field.to_string(), get_domain(), e.what() );
			}

		}

		virtual std::string
		get_domain() const override
		{
			return fn_name + arg_domains;
		}

		std::string
		get_fn_name() const
		{
			return fn_name;
		}

	  private:

		std::string fn_name;
		std::string arg_domains;
		std::function< T( field_t ) > parser_fn;
	};

	template< typename T >
	class parse_registry : public parser_t< T >
	{
	  public:

		parse_registry( parse_registry< T >* outer_context = nullptr )
			: outer_context( outer_context )
		{}

		template< typename ... Args >
		void
		register_fn( const std::function< T( Args ... ) >& fn, const std::string& fn_name, const assignment_config< Args >& ... configs )
		{
			fn_parser_t< T > fn_parser( fn, fn_name, configs ... );
			fn_registry.emplace( fn_parser.get_fn_name(), fn_parser );
		}

		// TODO remove this hack to get register_fn working with empty parameter pack
		void
		register_fn( const std::function< T() >& fn, const std::string& fn_name )
		{
			fn_parser_t< T > fn_parser( fn, fn_name );
			fn_registry.emplace( fn_parser.get_fn_name(), fn_parser );
		}

		void
		register_var( const std::string& var_name, const T& value )
		{
			if( is_varname( field_t( var_name ) ) )
			{
				var_registry[var_name] = value;
			}
			else
			{
				throw std::runtime_error( "Tried to register a variable \"" + var_name + "\" which is not in the form of a variable name." );
			}
		}

		void
		register_value( std::shared_ptr< parser_t< T > > value_parser )
		{
			if( !value_parser )
			{
				throw std::runtime_error( "Tried to register a nullptr value parser." );
			}

			this->value_parser = value_parser;
		}

		virtual T
		parse( field_t field ) const
		{
			try
			{
				field = trim_whitespace( field );

				std::string field_str = field.to_string();

				if( var_registry.count( field.to_string() ) )
				{
					return var_registry.at( field.to_string() );
				}

				field_t fn_name = trim_whitespace( split( field, std::string( 1, paren_delims.first ), all_block_delims, false ).first );

				if( fn_registry.count( fn_name.to_string() ) )
				{
					return fn_registry.at( fn_name.to_string() ).parse( field );
				}

				if( value_parser )
				{
					return value_parser->parse( field );
				}

				if( outer_context )
				{
					return outer_context->parse( field );
				}
			}
			catch( const parse_exception& e )
			{
				throw parse_exception( field.to_string(), get_domain(), e.what() );
			}

			throw parse_exception( field.to_string(), get_domain(), "Couldn't be parsed." );
		}

		void
		set_domain( const std::string& domain_name )
		{
			this->domain_name = domain_name;
		}

		virtual
		std::string
		get_domain() const override
		{
			return domain_name;
		}

		std::vector< std::string >
		get_vars() const
		{
			std::vector< std::string > vars;
			std::transform( var_registry.begin(), var_registry.end(), std::back_inserter( vars ),
			                []( const std::pair< std::string, T >& var_val )
							{
								return var_val.first;
							}
			);

			return vars;
		}

		std::vector< std::string >
		get_fns() const
		{
			std::vector< std::string > fns;
			std::transform( fn_registry.begin(), fn_registry.end(), std::back_inserter( fns ),
			                []( const std::pair< std::string, fn_parser_t< T > >& registered_fn )
							{
								return registered_fn.second.get_domain();
							}
			);

			return fns;
		}

	  private:
		std::map< std::string, fn_parser_t< T > > fn_registry;

		std::map< std::string, T > var_registry;
		std::shared_ptr< parser_t< T > > value_parser;
		std::string domain_name;

		parse_registry< T >* outer_context;
	};

	template< typename T >
	struct global_parser_registry_t
	{
		static std::shared_ptr< parse_registry< T > > registry;
	};

	template< typename T >
	std::shared_ptr< parse_registry< T > >
	global_parser_registry_t< T >::registry( new parse_registry< T > );


	template< typename T >
	void
	register_default_value_parser()
	{
		auto parser = std::make_shared< default_parser_t< T > >();
		global_parser_registry_t< T >::registry->register_value(
			parser
		);

		global_parser_registry_t< T >::registry->set_domain(
			parser->get_domain()
		);
	}

	template< typename ... T >
	void
	register_default_value_parsers()
	{
		std::make_tuple(
			(
				(void) register_default_value_parser< T >(),
				0
			) ...
		);
	}

}
