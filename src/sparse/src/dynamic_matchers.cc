
#include "../include/sparse/dynamic_matchers.h"

using namespace sparse::dynamic;


/* matcher_t */

matcher_t::matcher_t( std::unique_ptr< matcher_i > impl )
	: impl( std::move( impl ) )
{}

matcher_t::matcher_t( const matcher_t& other )
	: impl( other.impl->clone() )
{}

matcher_t::matcher_t( matcher_t && other )
	: impl( std::move( other.impl ) )
{}

matcher_t&
matcher_t::operator=( const matcher_t& other )
{
	impl = other.impl->clone();
	return *this;
}

matcher_t&
matcher_t::operator=( matcher_t&& other )
{
	impl = std::move( other.impl );
	return *this;
}

bool
matcher_t::visit( field_t field, const std::function< bool(field_t) >& op ) const
{
	return impl->visit( field, op );
}

std::string
matcher_t::domain() const
{
	return impl->domain();
}

matcher_t
matcher_t::operator+( matcher_t other ) const
{
	if( const auto* join_ptr = dynamic_cast< const join_t* >( impl.get() ) )
	{
		return matcher_t{ std::unique_ptr< matcher_i >{ new join_t{ *join_ptr + std::move( other ) } } };
	}
	else
	{
		return matcher_t{ std::unique_ptr< matcher_i >{ new join_t( { *this, std::move( other ) } ) } };
	}
}

matcher_t
matcher_t::operator|( matcher_t other ) const
{
	if( const auto* or_ptr = dynamic_cast< const or_t* >( impl.get() ) )
	{
		return matcher_t{ std::unique_ptr< matcher_i >{ new or_t{ *or_ptr | std::move( other ) } } };
	}
	else if( dynamic_cast< const or_t* >( other.impl.get() ) )
	{
		return matcher_t{ std::unique_ptr< matcher_i >{ new or_t{ or_t{ {*this} } | std::move( other ) } } };
	}
	else
	{
		return matcher_t{ std::unique_ptr< matcher_i >{ new or_t( { *this, std::move( other ) } ) } };
	}
}

/* literal_t */

bool
literal_t::visit( field_t field, const std::function< bool(field_t) >& op ) const
{
	if( val.size() > field.size() )
	{
		return false;
	}
	auto result = std::mismatch( val.begin(), val.end(), field.begin() );
	if( result.first != val.end() )
	{
		return false;
	}
	return op( field_t{ result.second, field.end() } );
}

std::string
literal_t::domain() const
{
	return val;
}

std::unique_ptr< matcher_i >
literal_t::clone() const
{
	return std::unique_ptr< matcher_i >{ new literal_t{ val } };
}


/* wild_t */

wild_t::wild_t()
{}

bool
wild_t::visit( field_t field, const std::function< bool(field_t) >& op ) const
{
	if( field.size() > 0 && *field.begin() != '\n' ) // Don't match newline
	{
		return op( field_t{ std::next( field.begin() ), field.end() } );
	}
	return false;
}

std::string
wild_t::domain() const
{
	return ".";
}

std::unique_ptr< matcher_i >
wild_t::clone() const
{
	return std::unique_ptr< matcher_i >{ new wild_t{} };
}

/* end_of_field_t */
end_of_field_t::end_of_field_t()
{}

bool
end_of_field_t::visit( field_t field, const std::function< bool(field_t) >& op ) const
{
	if( field.begin() == field.end() )
	{
		return op( field );
	}
	return false;
}

std::string
end_of_field_t::domain() const
{
	return "$";
}

std::unique_ptr< matcher_i >
end_of_field_t::clone() const
{
	return std::unique_ptr< matcher_i >{ new end_of_field_t{} };
}

/* join_t */

join_t::join_t( std::vector< matcher_t > matchers )
	: matchers( std::move( matchers ) )
{}

bool
join_t::visit( field_t field, const std::function< bool(field_t) >& op ) const
{
	return _visit( field, op, 0u );
}

bool
join_t::_visit( field_t field, const std::function< bool(field_t) >& op, size_t current_matcher_i ) const
{
	if( current_matcher_i == matchers.size() )
	{
		return op( field );
	}

	return matchers[current_matcher_i].visit(
		field,
		[&]( field_t unmatched_by_i )
		{
			return _visit( unmatched_by_i, op, current_matcher_i + 1u );
		}
	);
}

std::string
join_t::domain() const
{
	std::stringstream ss;
	for( const auto& matcher : matchers )
	{
		ss << matcher.domain();
	}
	return ss.str();
}

std::unique_ptr< matcher_i >
join_t::clone() const
{
	return std::unique_ptr< matcher_i >{ new join_t{ matchers } };
}

join_t
join_t::operator+( matcher_t other ) const
{
	if( auto* join_ptr = dynamic_cast< join_t* >( other.get() ) )
	{
		std::vector< matcher_t > matchers_plus_other = matchers;
		matchers_plus_other.insert( matchers_plus_other.end(), std::make_move_iterator( join_ptr->matchers.begin() ), std::make_move_iterator( join_ptr->matchers.end() ) );
		return join_t{ std::move( matchers_plus_other ) };
	}
	else
	{
		std::vector< matcher_t > matchers_plus_other = matchers;
		matchers_plus_other.emplace_back( std::move( other ) );
		return join_t{ std::move( matchers_plus_other ) };
	}
}

/* or_t */

or_t::or_t( std::vector< matcher_t > matchers )
	: matchers( std::move( matchers ) )
{}

bool
or_t::visit( field_t field, const std::function< bool(field_t) >& op ) const
{
	for( const auto& matcher : matchers )
	{
		if( matcher.visit( field, op ) )
		{
			return true;
		}
	}
	return false;
}

std::string
or_t::domain() const
{
	std::stringstream ss;
	ss << "(";
	for( auto it = matchers.begin(); it != matchers.end(); ++it )
	{
		ss << it->domain();
		if( std::next( it ) != matchers.end() )
		{
			ss << "|";
		}
	}
	ss << ")";
	return ss.str();
}

std::unique_ptr< matcher_i >
or_t::clone() const
{
	return std::unique_ptr< matcher_i >{ new or_t{ matchers } };
}

or_t
or_t::operator|( matcher_t other ) const
{
	if( auto* or_ptr = dynamic_cast< or_t* >( other.get() ) )
	{
		std::vector< matcher_t > matchers_or_other = matchers;
		matchers_or_other.insert( matchers_or_other.end(), std::make_move_iterator( or_ptr->matchers.begin() ), std::make_move_iterator( or_ptr->matchers.end() ) );
		return or_t{ std::move( matchers_or_other ) };
	}
	else
	{
		std::vector< matcher_t > matchers_or_other = matchers;
		matchers_or_other.emplace_back( std::move( other ) );
		return or_t{ std::move( matchers_or_other ) };
	}
}

/* unordered_join_t */

unordered_join_t::unordered_join_t( matcher_t delimiter, std::vector< matcher_t > matchers )
	: delimiter( std::move( delimiter ) )
	, matchers( std::move( matchers ) )
{}

bool
unordered_join_t::visit( field_t field, const std::function< bool(field_t) >& op ) const
{
	std::vector< size_t > unmatched;
	for( size_t i = 0u; i < matchers.size(); ++i )
	{
		unmatched.emplace_back( i );
	}
	return _visit( field, op, std::move( unmatched ) );
}

bool
unordered_join_t::_visit( field_t field, const std::function< bool(field_t) >& op, std::vector< size_t > unmatched_indices ) const
{
	if( unmatched_indices.empty() )
	{
		return op( field );
	}

	for( auto it = unmatched_indices.begin(); it != unmatched_indices.end(); ++it )
	{
		bool matched_i_unordered_join_the_rest = matchers[*it].visit(
			field,
			[&]( field_t unmatched_by_i )
			{
				std::vector< size_t > unmatched_indices_excluding_i;
				unmatched_indices_excluding_i.insert( unmatched_indices_excluding_i.end(), unmatched_indices.begin(), it );
				unmatched_indices_excluding_i.insert( unmatched_indices_excluding_i.end(), std::next( it ), unmatched_indices.end() );

				if( !unmatched_indices.empty() )
				{
				    return delimiter.visit(
						unmatched_by_i,
						[&]( field_t unmatched_by_i_and_delim )
						{
							return _visit( unmatched_by_i_and_delim, op, unmatched_indices_excluding_i );
						}
				    );
				}
				else
				{
				    return _visit( unmatched_by_i, op, std::move( unmatched_indices_excluding_i ) );
				}
			}
		);

		if( matched_i_unordered_join_the_rest )
		{
			return true;
		}
	}
	return false;
}

std::string
unordered_join_t::domain() const
{
	std::stringstream ss;
	ss << "(";
	for( auto it = matchers.begin(); it != matchers.end(); ++it )
	{
		ss << it->domain();
		if( std::next( it ) != matchers.end() )
		{
			ss << "&";
		}
	}
	ss << ")";
	return ss.str();
}

std::unique_ptr< matcher_i >
unordered_join_t::clone() const
{
	return std::unique_ptr< matcher_i >{ new unordered_join_t{ delimiter, matchers } };
}

/* override_domain_t */

override_domain_t::override_domain_t( const std::string& forced_domain, matcher_t matcher )
	: forced_domain( forced_domain )
	, matcher( std::move( matcher ) )
{}

bool
override_domain_t::visit( field_t field, const std::function< bool(field_t) >& op ) const
{
	return matcher.visit( field, op );
}

std::string
override_domain_t::domain() const
{
	return forced_domain;
}

std::unique_ptr< matcher_i >
override_domain_t::clone() const
{
	return std::unique_ptr< matcher_i >{ new override_domain_t{ forced_domain, matcher } };
}

/* repeat_between_t */

repeat_between_t::repeat_between_t( size_t min, size_t max, matcher_t matcher )
	: min{ min }
	, max{ max }
	, matcher{ std::move( matcher ) }
{}

bool
repeat_between_t::visit( field_t field, const std::function< bool(field_t) >& op ) const
{

	// The empty string is considered a match
	if( min == 0u )
	{
		if( op( field ) )
		{
			// early exit if the operation tells us to
			return true;
		}
	}
	std::list< std::pair< field_t, size_t > > unmatched_stack;
	unmatched_stack.emplace_back( field, 0u );


	while( !unmatched_stack.empty() )
	{
		field_t unmatched = std::move( unmatched_stack.back().first );
		size_t depth      = unmatched_stack.back().second;
		unmatched_stack.pop_back();

		if( depth + 1 > max )
		{
			continue;
		}

		bool matched = matcher.visit(
			unmatched,
			[&]( field_t child_unmatched )
			{
				unmatched_stack.emplace_back( child_unmatched, depth + 1 );
				if( min <= depth + 1 )
				{
				    return op( child_unmatched );
				}
				else
				{
				    return false;
				}
			}
		);

		if( matched )
		{
			return true;
		}
	}
	return false;
}

std::string
repeat_between_t::domain() const
{
	std::stringstream ss;
	ss << matcher.domain() << "{" << min << "," << max << "}";
	return ss.str();
}

std::unique_ptr< matcher_i >
repeat_between_t::clone() const
{
	return std::unique_ptr< matcher_i >{ new repeat_between_t{ min, max, matcher } };
}

/* free functions */

matcher_t
sparse::literal( std::string val )
{
	return dynamic::matcher_t{ std::unique_ptr< dynamic::matcher_i >( new dynamic::literal_t{ std::move( val ) } ) };
}

matcher_t
sparse::wild()
{
	return dynamic::matcher_t{ std::unique_ptr< dynamic::matcher_i >( new dynamic::wild_t{} ) };
}

///
/// @brief Produces a matcher which matches the end of the inputted field.
///
matcher_t
sparse::end_of_field()
{
	return dynamic::matcher_t{ std::unique_ptr< dynamic::matcher_i >( new dynamic::end_of_field_t{} ) };
}

matcher_t
sparse::override_domain( const std::string& forced_domain, dynamic::matcher_t matcher )
{
	return dynamic::matcher_t{ std::unique_ptr< dynamic::matcher_i >{ new dynamic::override_domain_t{ forced_domain, std::move( matcher ) } } };
}

matcher_t
sparse::repeat_between( size_t min, size_t max, dynamic::matcher_t matcher )
{
	return dynamic::matcher_t{ std::unique_ptr< dynamic::matcher_i >{ new dynamic::repeat_between_t{ min, max, std::move( matcher ) } } };
}

matcher_t
sparse::repeat_up_to( size_t n, dynamic::matcher_t matcher )
{
	return repeat_between( 0u, n, std::move( matcher ) );
}

matcher_t
sparse::maybe( dynamic::matcher_t matcher )
{
	std::string orig_domain = matcher.domain();
	return override_domain( orig_domain + "?", repeat_between( 0u, 1u, std::move( matcher ) ) );
}

matcher_t
sparse::repeat_exactly( size_t n, dynamic::matcher_t matcher )
{
	std::string orig_domain = matcher.domain();
	std::stringstream forced_domain;
	forced_domain << orig_domain << "{" << n << "}";
	return override_domain( forced_domain.str(), repeat_between( n, n, std::move( matcher ) ) );
}

matcher_t
sparse::repeat_any( dynamic::matcher_t matcher )
{
	std::string orig_domain = matcher.domain();
	return override_domain( orig_domain + "*", repeat_up_to( std::numeric_limits< size_t >::max(), std::move( matcher ) ) );
}

matcher_t
sparse::repeat_at_least_once( dynamic::matcher_t matcher )
{
	std::string orig_domain = matcher.domain();
	return override_domain( orig_domain + "+", repeat_between( 1u, std::numeric_limits< size_t >::max(), std::move( matcher ) ) );
}

matcher_t
sparse::whitespaces()
{
	return override_domain( "\\s", literal( " " ) | literal( "\t" ) | literal( "\n" ) | literal( "\f" ) | literal( "\r" ) | literal( "\v" ) );
}

matcher_t
sparse::ws()
{
	return repeat_any( whitespaces() );
}