
#include "../include/sparse/parse_exception.h"

using namespace sparse;

parse_exception::parse_exception( const std::string& parse_str, const std::string& domain_str, const std::string& reason )
	: std::runtime_error( "Failed to parse \"" + parse_str + "\" using the parser for \"" + domain_str + "\":\n" + reason )
	, parse_str( parse_str )
	, domain_str( domain_str )
	, reason( reason )
{}

const std::string&
parse_exception::get_field() const
{
	return parse_str;
}

const std::string&
parse_exception::get_domain() const
{
	return domain_str;
}

const std::string&
parse_exception::get_reason() const
{
	return reason;
}
