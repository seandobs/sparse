
#include "../include/sparse/sparse.h"

using namespace sparse;

template< typename Iterator >
Iterator
sparse::skip_delim( Iterator begin, Iterator end, Iterator delim_begin, Iterator delim_end )
{
	auto it       = begin;
	auto delim_it = delim_begin;

	while( it != end
	       && delim_it != delim_end
	       && *it == *delim_it )
	{
		++it;
		++delim_it;
	}

	if( delim_it == delim_end )
	{
		return it;
	}
	else
	{
		return begin;
	}
}

std::string::const_iterator
sparse::skip_block( std::string::const_iterator begin, std::string::const_iterator end, std::pair< char, char > block_delim )
{
	if( begin != end )
	{
		if( *begin == block_delim.first )
		{
			size_t count_match = 1u;
			++begin;

			while( count_match > 0u && begin != end )
			{
				if( *begin == block_delim.second )
				{
					count_match--;
				}
				else if( *begin == block_delim.first )
				{
					count_match++;
				}

				++begin;
			}
		}
	}

	return begin;
}

std::pair< field_t, field_t >
sparse::split( field_t field, const std::string& delimiter, const std::set< std::pair< char, char > >& block_delims, bool purge_delim )
{
	auto first_half_end    = field.begin();
	auto second_half_begin = field.end();

	while( first_half_end != field.end() )
	{


		auto next_begin = skip_delim( first_half_end, field.end(), delimiter.begin(), delimiter.end() );
		if( next_begin != first_half_end )
		{
			if( purge_delim )
			{
				second_half_begin = next_begin;
			}
			else
			{
				second_half_begin = first_half_end;
			}
			break;
		}

		auto skipped = first_half_end;
		for( const std::pair< char, char >& block_delim : block_delims )
		{
			skipped = skip_block( skipped, field.end(), block_delim );
		}

		if( skipped != first_half_end )
		{
			first_half_end = skipped;
		}
		else if( first_half_end != field.end() )
		{
			++first_half_end;
		}
	}

	return {
			   { field.begin(), first_half_end },
			   { second_half_begin, field.end() }
	};
}

std::vector< field_t >
sparse::tokenize( field_t field, const std::string& delimiter, const std::set< std::pair< char, char > >& block_delims )
{
	std::vector< field_t > fields;

	std::pair< field_t, field_t > first_rest = split( field, delimiter, block_delims );
	while( first_rest.first.begin() != first_rest.second.end() )
	{
		fields.push_back( first_rest.first );
		first_rest = split( first_rest.second, delimiter, block_delims );
	}

	return fields;
}

std::vector< field_t >
sparse::tokenize( field_t field, const std::set< std::string >& delimiters, const std::set< std::pair< char, char > >& block_delims )
{
	std::vector< field_t > fields = { field };

	for( const std::string delimiter : delimiters )
	{
		std::vector< field_t > next_fields;
		for( field_t token : fields )
		{
			std::vector< field_t > tokens = tokenize( token, delimiter, block_delims );
			next_fields.insert( next_fields.end(), tokens.begin(), tokens.end() );
		}
		fields = next_fields;
	}

	return fields;
}

std::string
sparse::join( const std::vector< field_t >& fields, const std::string& separator )
{
	std::stringstream ss;

	for( size_t i = 0u; i < fields.size(); ++i )
	{
		ss << std::string( fields[i].begin(), fields[i].end() );
		if( i != fields.size() - 1 )
		{
			ss << separator;
		}
	}

	return ss.str();
}

std::string
sparse::join_non_empty( const std::vector< field_t >& fields, const std::string& separator )
{
	std::vector< field_t > non_empty_fields;
	for( const field_t& field : fields )
	{
		if( field.begin() != field.end() )
		{
			non_empty_fields.push_back( field );
		}
	}

	return join( non_empty_fields, separator );
}



std::string
sparse::strip( field_t field, const std::set< std::string >& delimiters, const std::set< std::pair< char, char > >& block_delims )
{
	return join( tokenize( field, delimiters, block_delims ) );
}

std::string
sparse::strip_whitespace( field_t field, const std::set< std::pair< char, char > >& block_delims )
{
	return strip( field, whitespace, block_delims );
}

std::string
sparse::squeeze( field_t field, const std::set< std::string >& delimiters, const std::set< std::pair< char, char > >& block_delims, const std::string& separator )
{
	return join_non_empty( tokenize( field, delimiters, block_delims ), separator );
}

std::string
sparse::squeeze_whitespace( field_t field, const std::set< std::pair< char, char > >& block_delims )
{
	return squeeze( field, whitespace, block_delims, " " );
}

field_t
sparse::trim( field_t field, const std::set< std::string >& delimiters )
{
	auto trimmed_begin = field.begin();

	while( trimmed_begin != field.end() )
	{
		bool any_match = false;

		for( const std::string& delimiter : delimiters )
		{
			auto next_begin = skip_delim( trimmed_begin, field.end(), delimiter.begin(), delimiter.end() );
			if( next_begin != trimmed_begin )
			{
				trimmed_begin = next_begin;
				any_match     = true;
			}
		}

		if( !any_match )
		{
			break;
		}
	}

	auto trimmed_end = field.rbegin();
	while( trimmed_end != field.rend() )
	{
		bool any_match = false;

		for( const std::string& delimiter : delimiters )
		{
			auto next_end = skip_delim( trimmed_end, field.rend(), delimiter.rbegin(), delimiter.rend() );
			if( next_end != trimmed_end )
			{
				trimmed_end = next_end;
				any_match   = true;
			}
		}

		if( !any_match )
		{
			break;
		}
	}

	return field_t( trimmed_begin, trimmed_end.base() );
}

field_t
sparse::trim_whitespace( field_t field )
{
	return trim( field, whitespace );
}


field_t
sparse::trim_block( field_t field, const std::pair< char, char >& block_delim )
{
	field = trim_whitespace( field );

	auto begin = field.begin();
	if( begin != field.end()
	    && *begin == block_delim.first )
	{
		++begin;
	}

	auto rbegin = field.rbegin();
	auto rend   = std::reverse_iterator< std::string::const_iterator >( begin );
	if( rbegin != rend
	    && *rbegin == block_delim.second )
	{
		++rbegin;
	}

	return { begin, rbegin.base() };
}

bool
sparse::is_varname( field_t field )
{
	if( field.begin() == field.end() )
	{
		return false;
	}

	if( !( isalpha( *field.begin() ) || *field.begin() == '_' ) )
	{
		return false;
	}

	for( auto it = field.begin(); it != field.end(); ++it )
	{
		if( !( isalnum( *it ) || *it == '_' ) )
		{
			return false;
		}
	}

	return true;
}

bool
sparse::is_block( field_t field, const std::pair< char, char >& block_delim )
{
	if( field.begin() == field.end()
	    || field.begin() == field.rbegin().base() )
	{
		return false;
	}

	return *field.begin() == block_delim.first
	       && *field.rbegin() == block_delim.second;
}

std::pair< field_t, field_t >
sparse::assignment_split( field_t field, const std::set< std::pair< char, char > >& block_delims )
{
	std::pair< field_t, field_t > assignment = split( field, "=", block_delims );

	field_t trimmed_varname = trim_whitespace( assignment.first );

	field_t trimmed_val = trim_whitespace( assignment.second );

	return { trimmed_varname, trimmed_val };
}
