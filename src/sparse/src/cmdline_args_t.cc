#include "../include/sparse/cmdline_args_t.h"


#include <algorithm>

#include "../include/sparse/parse_exception.h"

#include "../include/sparse/matchers.h"

using namespace sparse;

namespace
{
	template< typename matcher_t >
	std::optional< std::string >
	pop_param_impl( std::list< std::string >& args, const matcher_t& matcher )
	{
		std::optional< std::string > result;
		for( auto it = args.begin(); it != args.end(); )
		{
			auto next_it = std::next( it );
			matcher.visit(
				*it,
				[&]( field_t )
				{
					if( next_it == args.end() )
					{
					    throw sparse::parse_exception( "", matcher.domain(), "Expected a string parameter following the given argument, but none was provided." );
					}

					args.erase( it );
					it      = next_it;
					next_it = std::next( next_it );

					result = *it;
					args.erase( it );
					return true;
				}
			);
			it = next_it;
		}
		return result;
	}
}

cmdline_args_t::cmdline_args_t( int argc, char** argv )
	: executable_name{ argv[0u] }
{
	for( int i = 1; i < argc; ++i )
	{
		args.emplace_back( argv[i] );
	}
}

cmdline_args_t::cmdline_args_t( std::string executable_name, std::list< std::string > args )
	: executable_name{ std::move( executable_name ) }
	, args{ std::move( args ) }
{}

std::string
cmdline_args_t::get_domain_for_param( const std::string& longform, const char shortform )
{
	return
	    std::string( "[" )
	    + "[--" + longform + ( shortform != '\0' ? std::string( "|-" ) + shortform : std::string( "" ) ) + "]"
	    + " STRING"
	    + "]";
}

std::optional< std::string >
cmdline_args_t::pop_param( const std::string& longform, const char shortform )
{
	if( shortform != '\0' )
	{
		auto matcher = override_domain(
			get_domain_for_param( longform, shortform ),
			( ( literal( "--" + longform ) )
			  | ( literal( "-" + std::string( 1u, shortform ) ) ) )
			+ end_of_field()
		);

		return pop_param_impl( args, matcher );
	}
	else
	{
		auto matcher = override_domain(
			get_domain_for_param( longform, shortform ),
			literal( "--" + longform ) + end_of_field()
		);
		return pop_param_impl( args, matcher );
	}
}

std::string
cmdline_args_t::pop_required_param( const std::string& longform, const char shortform )
{
	std::optional< std::string > result = pop_param( longform, shortform );
	if( !result )
	{
		throw sparse::parse_exception( "", get_domain_for_param( longform, shortform ), "The argument is required, but it was not supplied." );
	}
	return *result;
}

bool
cmdline_args_t::pop_flag( const std::string& longform, const char shortform )
{
	bool result = false;

	for( auto it = args.begin(); it != args.end(); )
	{
		auto next_it = std::next( it );

		if( *it == "--" + longform )
		{
			result = true;
			args.erase( it );
		}
		else if( shortform != '\0' && it->size() >= 2u && it->at( 0u ) == '-' && it->at( 1u ) != '-' )
		{
			auto found_shortform_it = std::remove( it->begin(), it->end(), shortform );
			if( found_shortform_it != it->end() )
			{
				result = true;
				it->erase( found_shortform_it, it->end() );
			}
			if( it->size() == 1u )
			{
				args.erase( it );
			}
		}

		it = next_it;
	}

	return result;
}

void
cmdline_args_t::finalize() const
{
	if( !args.empty() )
	{
		std::string unknown_args;
		for( auto it = args.begin(); it != args.end(); ++it )
		{
			unknown_args += *it;
			if( std::next( it ) != args.end() )
			{
				unknown_args += " ";
			}
		}
		throw sparse::parse_exception( unknown_args, "", "Unknown arguments." );
	}
}
