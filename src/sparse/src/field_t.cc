#include "../include/sparse/field_t.h"

using namespace sparse;

field_t::field_t( std::string::const_iterator begin, std::string::const_iterator end )
	: begin_it( begin )
	, end_it( end )
{}

field_t::field_t( const std::string& str )
	: begin_it( str.begin() )
	, end_it( str.end() )
{}

std::string
field_t::to_string() const
{
	return std::string( begin(), end() );
}

std::string::const_iterator
field_t::begin() const
{
	return begin_it;
}

std::string::const_iterator
field_t::end() const
{
	return end_it;
}

std::string::const_reverse_iterator
field_t::rbegin() const
{
	return std::reverse_iterator< std::string::const_iterator >( end() );
}

std::string::const_reverse_iterator
field_t::rend() const
{
	return std::reverse_iterator< std::string::const_iterator >( begin() );
}

size_t
field_t::size() const
{
	return static_cast< size_t >( end() - begin() );
}

bool
field_t::empty() const
{
	return begin() == end();
}

bool
field_t::operator==( const field_t& other ) const
{
	return begin() == other.begin() && end() == other.end();
}

bool
field_t::operator!=( const field_t& other ) const
{
	return !operator==( other );
}